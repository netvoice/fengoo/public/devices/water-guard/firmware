#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""wsagent.py:program pro odesílání a příjem přes WebSocket"""
__author__	= "Ladislav Hlousek"
__copyright__	= "Copyright 2018, Ladislav Hlousek"
__version__	= "1.0.0"
__maintainer__	= "Ladislav Hlousek"
__email__	= "ladishlousek@gmail.com"
__status__	= "Production"

import os,sys,signal,requests
import websocket
import psutil
import base64
import json
from time import time, sleep
from datetime import datetime
import threading
import sqlite3

config = {}

ADDRESS = ""
CREDENTIALS = ""

db = "/home/pi/ChytryVodomer/db/chytryvodomer.db"

writer = sqlite3.connect(db, isolation_level=None, check_same_thread=False)
writer.row_factory = lambda c, r: dict(zip([col[0] for col in c.description], r))
writer.execute('''PRAGMA journal_mode=WAL;''')
writer.commit()

reader = sqlite3.connect(db, isolation_level=None, check_same_thread=False)
reader.row_factory = lambda c, r: dict(zip([col[0] for col in c.description], r))
reader.execute('''PRAGMA journal_mode=WAL;''')
reader.commit()

curVersion = "1.0.4"

def isConnected():
	url = "https://www.google.com"
	timeout = 5
	res = False
	try:
		request = requests. get(url, timeout=timeout)
		res = True
	except (requests. ConnectionError, requests. Timeout) as exception:
		write_log("WARNING", "Application", "No internet connection")
		res = False
		pass
	return res

def measure_temp():
	temp = os.popen("vcgencmd measure_temp").readline()
	temp = temp.replace("temp=","")
	temp = temp.replace("'C","")
	return temp
	
def measure_core_volts():
	volts = os.popen("vcgencmd measure_volts core").readline()
	volts = volts.replace("volt=","")
	volts = volts.replace("V","")
	return volts

def measure_cpu():
	return psutil.cpu_percent()

def measure_memory(free=False):
	memory = psutil.virtual_memory()
	# Divide from Bytes -> KB -> MB
	available = round(memory.available/1024.0/1024.0,1)
	total = round(memory.total/1024.0/1024.0,1)
	if free == True:
		return available
	else:
		return total

def write_log(log_type, log_title, log_data):
	file_name = "/home/pi/ChytryVodomer/logs/wsagent.log"
	lines = []
	if os.path.exists(file_name) == False:
		open(file_name, "x")
	else:
		with open(file_name, "r") as file:
			lines = file.read()
	
	now = datetime.now()
	dt_string = now.strftime("%d.%m.%Y %H:%M:%S.%f")
	# dd.mm.YY H:M:S.milisec
	
	with open(file_name, "w") as f:
		if(len(lines) > 10000):
			lines = lines[:-1]
		if(len(lines) > 0):
			f.writelines(lines)
		
		f.write(log_type + ";" + dt_string + ";" + log_title + ";" + log_data + "\n")

def signal_handler(signal, frame):
	write_log("WARNING", "Application", "Closed by user input Ctrl+C")
	#print('Ctrl+C, exiting')
	sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

def getConfig():
	global config
	config = {
		'ID': 0,
		'LIMIT': 5,
		'JEDNOTKA': 1,
		"WS_SERVER": "device.fengoo.cz/ws/",
		"WS_KEY": "",
		"TOTAL_L": 0
	}

	cur = reader.cursor()
	cur.execute('''SELECT * FROM nastaveni_global ORDER BY Ulozeno DESC''')
	globalNastaveni = cur.fetchone()
	cur.close()
	if globalNastaveni:	
		if "ID" in globalNastaveni:
			config["ID"] = globalNastaveni["ID"]
			config["LIMIT"] = globalNastaveni["LimitLitry"]
			config["JEDNOTKA"] = globalNastaveni["LimitJednotka"]
			config["WS_SERVER"] = globalNastaveni["WSServer"]
			config["WS_KEY"] = globalNastaveni["WSKey"]
			config["TOTAL_L"] = globalNastaveni["PocatecniStavLitry"]


def updateConfig(limitJednotka = 0, limit = 0):
	global config
	if limitJednotka != None and limit != None and config["ID"] > 0:
		writer.execute('''UPDATE nastaveni_global SET LimitJednotka = ?, LimitLitry = ? WHERE ID = ?''', (limitJednotka, limit, config["ID"]))
		writer.commit()

def ventilCtrl(state):
	now = datetime.now()
	ts = datetime.timestamp(now)
	if state == 1:
		writer.execute('''INSERT INTO ventil_data (StavVentil, AutoAkce) VALUES (1,3)''')
	else:
		writer.execute('''INSERT INTO ventil_data (StavVentil, AutoAkce) VALUES (0,3)''')
	writer.commit()


def restartDev():
	os.system("sudo reboot")

#def updateDev():
#	os.system("sudo sh ./install.sh 2")


def sendResponse(ws,msgObj):
	message = json.dumps(msgObj)
	#print(message)
	ws.send(message.encode('utf-8'))


def on_open(ws):
	#write_log("INFO", "WSAGENT", "Connection open")
	startMsg = {
		"event": "started",
		"params": {
			"fwVersion": curVersion 
		}
	}
	sendResponse(ws,startMsg)
	
	def run():
		start_time = time()
		act_time = start_time
		first_send = True
		diff_skip = False
		while True:
			if first_send == True:
				diff_skip = True

			act_time = time()

			diff = act_time - start_time

			if diff_skip == True:
				diff = 120
				diff_skip = False
				first_send = False
				
			#print("SACT", start_time, act_time, diff)
			sleep(1)
			if diff >= 120:
				diff = 0
				start_time = time()
				global config
				getConfig()
				state = "CLOSED"
				pocetL = 0
				celkemL = 0
				prutok = 0
				limitType = config["JEDNOTKA"]
				limitValue = config["LIMIT"]
				cur = reader.cursor()
				cur.execute('''SELECT * FROM vodomer_data ORDER BY TsTime DESC''')
				akt_data = cur.fetchone()
				cur.close()
				cur = reader.cursor()
				cur.execute('''SELECT * FROM ventil_data ORDER BY TsTime DESC''')
				ventil = cur.fetchone()
				cur.close()
				
				cur = reader.cursor()
				cur.execute('''SELECT * FROM ventil_data WHERE StavVentil = 1 ORDER BY Ulozeno ASC LIMIT 1''')
				ventilStart = cur.fetchone()
				cur.close()
				
				if akt_data is not None:
					if "ID" in akt_data:
						pocetL = akt_data['PocetLitry']
						celkemL = akt_data['CelkemLitry']
						prutok =  akt_data['Prutok']
						prutok = round(prutok,1)

				celkemL = celkemL + config["TOTAL_L"]
				
				if ventil is not None:
					if "ID" in ventil:
						if ventil["StavVentil"] == 1:
							state = "OPEN"
						else:
							state = "CLOSED"

					if limitType == 1:
						limitType = "LITER"
					elif limitType == 2:
						limitType = "HOUR"
					elif limitType == 3:
						limitType = "MINUTE"
					elif limitType == 4:
						limitType = "LITER_PER_HOUR"
					elif limitType == 5:
						limitType = "LITER_PER_MINUTE"
					elif limitType == 0 or limitType == "":
						limitType = "NO_LIMIT"
						limitValue = 0
					else:
						limitType = "LITER"
				
				prvni_mereni = ""
				if ventilStart is not None:
					if "ID" in ventilStart:
						prvni_mereni = ventilStart["Ulozeno"]
						prvni_mereni = prvni_mereni.replace(" ","T")

				cpuUsage = 0.0
				memFree = 0.0
				memTotal = 0.0
				temp = 0.0
				volts = 0.0
				
				memTotal = measure_memory()
				memFree = measure_memory(True)
				cpuUsage = measure_cpu()
				temp = float(measure_temp())
				volts = float(measure_core_volts())
				
				message = json.dumps({"state": state, "consumption": pocetL, "totalConsumption": celkemL, "limitType": limitType, "limitValue": limitValue, "flow": prutok,
					"prvniMereni": prvni_mereni, "cpuUsage": cpuUsage, "memFree": memFree, "memTotal": memTotal, "temp": temp, "volts": volts
				})
				#print(message)
				isconn = isConnected()
				
				if isconn == True:
					ws.send(message.encode('utf-8'))
				
				diff = 0
				start_time = time()
				sleep(10)

	t = threading.Thread(target=run)
	t.start()


def on_message(ws, message):
	write_log("INFO", "WSAGENT", "WSMSG: " + message)
	msg = json.loads(message)
	sendResponse = 0
	responseMsg = {
		"event": "",
		"params": {
		}
	}
	#print(msg)

	if msg["command"] == "reset":
		responseMsg["event"] = "rebooting"
		sendResponse(ws,responseMsg)
		restartDev()

	#if msg["command"] == "update":
		#updateDev()

	if msg["command"] == "close":
		ventilCtrl(0)
		responseMsg["event"] = "closed"
		sendResponse = 1

	if msg["command"] == "openWithNoLimit":
		updateConfig(0, 0)
		ventilCtrl(1)
		responseMsg["event"] = "openedWithNoLimit"
		sendResponse = 1

	if msg["command"] == "openWithLiterLimit":
		if msg["params"]["value"] > 0:
			updateConfig(1,msg["params"]["value"])
			ventilCtrl(1)
			responseMsg["event"] = "openedWithLiterLimit"
			responseMsg["params"] = msg["params"]
			sendResponse = 1

	if msg["command"] == "openWithMinuteLimit":
		if msg["params"]["value"] > 0:
			updateConfig(3, msg["params"]["value"])
			ventilCtrl(1)
			responseMsg["event"] = "openedWithMinuteLimit"
			responseMsg["params"] = msg["params"]
			sendResponse = 1

	if msg["command"] == "openWithHourLimit":
		if msg["params"]["value"] > 0:
			updateConfig(2, msg["params"]["value"])
			ventilCtrl(1)
			responseMsg["event"] = "openedWithHourLimit"
			responseMsg["params"] = msg["params"]
			sendResponse = 1

	if msg["command"] == "openWithLiterPerMinuteLimit":
		if msg["params"]["value"] > 0:
			updateConfig(5, msg["params"]["value"])
			ventilCtrl(1)
			responseMsg["event"] = "openedWithLiterPerMinuteLimit"
			responseMsg["params"] = msg["params"]
			sendResponse = 1

	if msg["command"] == "openWithLiterPerHourLimit":
		if msg["params"]["value"] > 0:
			responseMsg["event"] = "openedWithLiterPerHourLimit"
			responseMsg["params"] = msg["params"]
			sendResponse = 1
	
	if msg["command"] == "updateFirmware":
		os.system("sudo sh /home/pi/install.sh 8")
		#if msg["params"]["version"] != curVersion:
		#	responseMsg["event"] = "updateFirmware"
		#	responseMsg["params"] = msg["params"]
			
		#	os.system("sudo sh /home/pi/install.sh 8")
		sendResponse = 0
			
	if sendResponse == 1:
		sendResponse(ws,responseMsg)


def on_close(ws):
	write_log("INFO", "WSAGENT", "WS Closed")

def on_error(ws, error):
	write_log("ERROR", "WSAGENT", "E: {0}".format(error))
	#print(error)
	sleep(2)

if __name__ == "__main__":
	write_log("INFO", "Application", "Started")
	getConfig()
	
	print(config)
	ADDRESS = "wss://device.fengoo.cz/ws/"
	CREDENTIALS = "b0xWY05ORkE6MTIzNDU="

	if config["WS_SERVER"] != "":
		ADDRESS = "wss://" + config["WS_SERVER"]

	if config["WS_KEY"] != "":
		CREDENTIALS = config["WS_KEY"]
	
	#print(config, ADDRESS, CREDENTIALS)
	with open('/home/pi/info/info.json',) as f:
		infoData = json.load(f)
		curVersion = infoData["Version"]
		
	websocket.enableTrace(True)
	ws = websocket.WebSocketApp(ADDRESS,
								on_message = on_message,
								on_error = on_error,
								on_close = on_close,
								on_open = on_open,
								header = {'Authorization: Basic ' + CREDENTIALS})
								#header = {'Authorization: Basic ' + base64.b64encode(bytes(CREDENTIALS,'utf-8')).decode('utf-8')})

	websocket.setdefaulttimeout=30
	while True:
		try:
			isconn = isConnected()
			if isconn == True:
				ws.run_forever(ping_interval=30, ping_timeout=10)
			else:
				sleep(60)
		except Exception as e:
			write_log("ERROR", "WSAGENT", "E: {0}".format(error))
			pass
