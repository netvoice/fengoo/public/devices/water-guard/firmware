#!/usr/bin/python3 test
"""Power.py - stará se o softwarové zapnutí a vypnutí"""
__author__      = "Pavel Vejnar"
__copyright__   = "Copyright 2019, Pavel Vejnar"
__version__     = "1.0.0"
__maintainer__  = "Pavel Vejnar"
__email__       = "vejnar.p@gmail.com"
__status__      = "Production"

import RPi.GPIO as GPIO
import sys,signal
from time import sleep
from datetime import datetime
import os

pin_ready = 33
pin_shutdown = 31


GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(pin_ready, GPIO.OUT)
GPIO.setup(pin_shutdown, GPIO.IN)
GPIO.output(pin_ready, GPIO.HIGH)


state = GPIO.input(pin_shutdown)

def write_log(log_type, log_title, log_data):
	file_name = "/home/pi/ChytryVodomer/logs/power.log"
	lines = []
	if os.path.exists(file_name) == False:
		open(file_name, "x")
	else:
		with open(file_name, "r") as file:
			lines = file.read()
	
	now = datetime.now()
	dt_string = now.strftime("%d.%m.%Y %H:%M:%S.%f")
	# dd.mm.YY H:M:S.milisec
	
	with open(file_name, "w") as f:
		if(len(lines) > 10000):
			lines = lines[:-1]
		if(len(lines) > 0):
			f.writelines(lines)
		
		f.write(log_type + ";" + dt_string + ";" + log_title + ";" + log_data + "\n")

def main():

	while True:
		state = GPIO.input(pin_shutdown)
		if state == 1:
			write_log("Power", "Shutdown", "Shutdown true")
			GPIO.output(pin_ready, GPIO.LOW)
			os.system("sudo shutdown -h now")
		sleep(1)


if __name__ == '__main__':
	write_log("Power", "Application", "started")
	main()