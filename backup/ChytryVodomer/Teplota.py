import os
import time
from datetime import datetime

def write_log(log_type, log_title, log_data):
	file_name = "/home/pi/ChytryVodomer/logs/tmplog.log"
	lines = []
	if os.path.exists(file_name) == False:
		open(file_name, "x")
	else:
		with open(file_name, "r") as file:
			lines = file.read()
	
	now = datetime.now()
	dt_string = now.strftime("%d.%m.%Y %H:%M:%S.%f")
	# dd.mm.YY H:M:S.milisec
	
	with open(file_name, "w") as f:
		if(len(lines) > 10000):
			lines = lines[:-1]
		if(len(lines) > 0):
			f.writelines(lines)
		
		f.write(log_type + ";" + dt_string + ";" + log_title + ";" + log_data + "\n")

def measure_temp():
	temp = os.popen("vcgencmd measure_temp").readline()
	return (temp.replace("temp=",""))

def main():
	write_log("INFO", "Application", "Started")
	while True:
		write_log("INFO","Temperature:",measure_temp())
		time.sleep(900)

if __name__ == '__main__':
	main()