#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sqlite3.h>
#include <time.h>
#include <wiringPi.h>

struct DevConfig {
    int limit;
	int limitType;
	int autoMod;
	int autoSendMail;
	int vychoziStav;
	int pulsLitr;
};

struct EventData {
	int id;
	char *title;
	char *start;
	char *end;
	int actionStart;
	int actionEnd;
	int limitType;
	int limitVal;
	int stavVentil;
	time_t t_Start;
	time_t t_End;
	int valid;
};

struct EmailConfig {
	char *SMTP_Host;
	int SMTP_Port;
	char *user;
	char *password;
	char *sender;
	char *receiver;
	char *security;
};

struct ActualData {
	int pocetLitry;
	int pocetLitryCelkem;
	int stavVentil;
	int autoMod;
};

char *str_replace(char* string, const char* substr, const char* replacement);

void clearDbResult(sqlite3_stmt *res, sqlite3 *db);

void printSQLVersion();

struct DevConfig getDevConfig();
void setDevConfig(struct DevConfig devCfg);

void updateLimit(struct EventData evt);

struct EventData findEvent();
void setEventVentil(struct EventData evt);

struct EmailConfig getEmailConfig();

struct ActualData getActualData();
void setActualData(struct ActualData actData);

int getStavVentil();
void setStavVentil(int stav, int autoMod);

int insertVodData(int pocetL, int celkemL, int prutok, int stavVentil);
void resetPrutok(int id);