# Vodoměr HW

* Výrobce: NetVoice s.r.o.
* Obchodní značka: Fengoo
* Výrobní číslo (formát): 8 znaků (číslice)
* Název zařízení: WATER GUARD
* Model: Fe-WTG-1-W
* Název WiFi (kde xxxx jsou první 4 znaky ze sn): Fengoo\_WATERGUARD\_xxxx
* Heslo WiFi: Fengoo99
* Název v eshopu CZ: Fengoo WATER GUARD - IoT chytrý vodoměr
* Název v eshopu AJ: Fengoo WATER GUARD - IoT smart watermeter
 
 

## Předběžné požadavky
* [wiki](https://git.netvoice.cz/iot/vodomer-hw/wikis/home)

## Prvotní instalace a připojení k Rpi
- Postup A(doporučeno):
  1. Stáhnout aktuální image pro vodomer a vytvořit bootovací SD kartu
  2. Po vložení karty do rpi a zapnutí by se po chvilce mělo zobrazit AP s názvem __Fengoo_WATERGUARD__. 
  3. Připojte se k WiFi __Fengoo_WATERGUARD__ s heslem __Fengoo99__.
  4. Navštivte web http://fengoo-waterguard.local:81 (192.168.4.1:81) pro výběr připojení k vaší WiFi síti.
  5. Po uložení se odpojte z WiFi sítě __Fengoo_WATERGUARD__ a připojte se zpět na svoji WiFi. Zařízení se během pár vteřin připojí k vaší WiFi síti.


- Postup B:
  1. Stáhnout aktuální raspbian image z raspbian.org a vytvořit bootovací SD kartu pomocí programu Win32DiskImager
  2. Jak připojit Rpi k wifi: 
     - Vložte SD kartu do PC a přejděte do její složky (zde bude vidět pár složek a souborů)
     - Zde vytvořte prázdný soubor __ssh__ bez přípony (jinak nepůjde ssh připojení)
     - Vytvořte další soubor  __wpa_supplicant.conf__ vložte následující a upravte podle vlastního wifi nastavení
     ```
      ctrl_interface=DIR/var/run/wpa_supplicant GROUP=netdev
      update_config=1
      Country=CZ
   
      network={
         ssid="nazev_wifi"
	       psk="heslo"
	       key_mgmt=WPA-PSK
      }
      ```
      - uložit - s formátem UTF, vložit zpět do Rpi a zapnout

  3. Poté je třeba ještě zjistit IP připojeného Rpi k WiFi třeba z routeru nebo pomocí nějakého programu na sken sítě.
  4. Pro vstup do rpi přes SSH (port 22) zadejte už. jméno: __pi__ heslo: __Fengoo@2020__


### Instalace závislostí (nutné jen u postupu B)
1. Instalace se provede kompletně automaticky stačí tento postup
   - __stáhnout__ (ne clone) z repozitáře soubor __install.sh__ a třeba přes program WinScp skrz SFTP protokol se přihlásist do Rpi (údaje stejné jako SSH) do složky /home/pi/ tento soubor zkopírovat
   - přihlásit do Rpi přes SSH a přejít do složky /home/pi/ příkazem __cd__ __/home/pi/__
   - zajdete příkaz __sudo__ __chmod__ __a+x__ __./install.sh__
   - zadejte příkaz __sudo__ __sh__ __./install.sh__ __1__ - tím se spustí instalace, nastavení DB, nastavení a spuštění služeb
     - Parametry pro skript : 1 - instalace, 2 - aktualizace, 3 - update pro rpi, 4 - reset logů db, služeb, 5 - stop služeb, 6 - update rpi FW a kernel
   - restart nastane automaticky
   - v prohlížeči zadat adresu Rpi. POZOR! Když je v režimu AP tak IP je - __192.168.4.1:81__
   - aplikace skoro vše loguje do složky /home/pi/ChytryVodomer/logs/ až na hotspot, ten loguje do své složky /home/pi/auto-hotspot/
2. Kdyby při instalaci nastal problém při stahování balíčku, tak nejspíše chybou kvůli DNS. Následující postup by měl problém vyřešit
  - ve složce __ChytryVodmer__ je soubor __resolv.conf__ a obsahuje definice pro DNS googlu
  - nejprveje je potřeba upravit práva pro zápis do souburu a to příkazem __sudo chown pi /etc/resolv.conf__ a __sudo chmod 777 /etc/resolv.conf__
  - poté překopírovat soubor do složky /etc/ a spustit instalaci znovu. POZOR! složka /home/pi musí obsahovat jenom soubor __install.sh__ všechny ostatní soubory a složky je třeba odstranit

## Změna názvu a hesla WiFi AP
1. Otevřít soubor třeba příkazem __sudo nano /home/pi/auto-hotspot/hostapd.conf__
2. Změnit hodnotu __ssid__ pro změnu názvu AP a __wpa_passphrase__ pro změnu hesla k AP
3. Uložit a restartovat RPI
   
## Příkazy pro zjištění stavu služeb
```sudo systemctl status wifiman``` - zjistí stav služby pro konfiguraci wifi sítě

```sudo systemctl status program``` - zjistí stav služby pro měření

```sudo systemctl status ventil``` - zjistí stav služby ventilu

```sudo systemctl status webapp``` - zjistí stav služby webové aplikace

```sudo systemctl status wsagent``` - zjistí stav služby websocket agenta

```sudo journalctl -xe``` - výpíše log ze služeb


## API

### Report

Příklady:
```json
{"state": "OPEN", "consumption": 54.0, "totalConsumption": 4287.0, "limitType": "LITER_PER_HOUR", "limitValue": 100, "flow": 10.5}
{"state": "OPEN", "consumption": 13.5, "totalConsumption": 2154.0, "limitType": "MINUTE", "limitValue": 15, "flow": 10.5}
{"state": "CLOSED", "consumption": 18.0, "totalConsumption": 16884.0, "limitType": "LITER", "limitValue": 18, "flow": 10.5}
{"state": "CLOSED", "consumption": 18.0, "totalConsumption": 16884.0, "limitType": "NO_LIMIT", "limitValue": 0, "flow": 10.5}
```

* `state` stav ventilu
  * `OPEN` - otevřeno
  * `CLOSED` - zavřeno
* `consumption` - aktuální spotřeba v litrech (dle `limitType`, v případě `NO_LIMIT` - počíta se vždy od 0 při otevření, `LITER` - počítá se dokud nebude dosaženo `limitValue`,
 `MINUTE` / `HOUR` -  počíta se vždy od 0 při otevření)
* `totalConsumption` - celková spotřeba v litrech (vždy v litrech bez ohledu na limitType) (od výroby pokuď )
* `limitType`
  * `NO_LIMIT` - není nastaveno žádné omezení, `limitValue` nemá žádný význam, je vždy 0
  * `LITER` - po spotřebování `limitValue` litrů vody od nastavení limitu se ventil uzavře, uzavřením / otevřením ventilu se hodnota `limitValue` nemění 
  * `MINUTE` - po uplynutí `limitValue` minut od nastavení limitu se ventilr uzavře, uzavřením ventilu se hodnota `limitValue` nemění 
  * `HOUR` - po uplynutí `limitValue` hodin od nastavení limitu se ventilr uzavře, uzavřením ventilu se hodnota `limitValue` nemění 
  * `LITER_PER_MINUTE` - totéž jako `LITER`, ale po celých minutách od nastavení limitu se čítač `consumption` vynuluje a ventil se znovu otevře do vyčerpání limitu
  * `LITER_PER_HOUR` - totéž jako `LITER`, ale po celých hodinách od nastavení limitu se čítač `consumption` vynuluje a ventil se znovu otevře do vyčerpání limitu
* `limitValue` - počet jednotek (litrů/minut/hodin - jednotka je dána hodnotou limitType)
* `flow` - aktuální průtok v litrech za minutu

Report se posílá každé __2 minuty__

### Povely pro ovládání

Pozn.: Každý povel pro ovládání ruší předchozí povel pro ovládání.

Uzavření ventilu
```json
{"command": "close", "params": {}}
```

Otevření limitu bez limitu
```json
{"command": "openWithNoLimit", "params": {}}
```

Otevření ventilu do spotřebování 150 litrů
```json
{"command": "openWithLiterLimit", "params": {"value": 150}}
```

Otevření ventilu na 45 minut
```json
{"command": "openWithMinuteLimit", "params": {"value": 45}}
```

Otevření ventilu na 6 hodin
```json
{"command": "openWithHourLimit", "params": {"value": 6}}
```

Otevření ventilu s maximální spotřebou 5 litrů za minutu (hodiny se počítají od okamžiku přijetí tohoto povelu)
```json
{"command": "openWithLiterPerMinuteLimit", "params": {"value": 5}}
```

Otevření ventilu s maximální spotřebou 40 litrů za hodinu (hodiny se počítají od okamžiku přijetí tohoto povelu)
```json
{"command": "openWithLiterPerHourLimit", "params": {"value": 40}}
```

### Povely pro plánovač

Pozn.: Spuštěná akce se chová stejně jako povel pro ovládání, tj. přeruší předchozí a je přerušena následujícím povelem pro ovládání resp. akcí plánovače.

```json
{
  "name": "addEvent",
  "params": {
    "name": "Název eventu",
    "start": "09.02.2019 14:00",
    "end": "09.02.2019 15:00",
    "startAction": {
      "name": "openWithNoLimit",
      "params": {}
    },
    "endAction": {
      "name": "close",
      "params": {}
    }
  }
}
```
* `celyDen` - př.: od 10.02.2019 00:00 do 10.02.2019 23:59 
* TODO pro plnohodnotné ovládání by to chtělo minimálně ještě možnost vypsat si naplánované události a smazat je. Mám ale za to, že plánovač je spíše pro offline použití vodoměru (tedy že se nebude ovládat přes náš web).
