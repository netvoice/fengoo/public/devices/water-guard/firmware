'use strict'

$( document ).ready(function() {
    $('body').append(modal);


    $('#logoutBtn').click(function () {
        $('#modalLogoutContainer').fadeIn(500)
    });

    $('#cancelLogoutBtn').click(function () {
        $('#modalLogoutContainer').fadeOut(500)
    });
})


// HTML Modalboxu
const modal = '' +
    '   <div id="modalLogoutContainer" class="UnauthorizedLayout" style="background-color: rgba(128, 128, 128, 0.5); z-index: 10000">\n' +
    '        <div id="modalLogout" class="UnauthorizedLayout-content" style="">\n' +
    '            <div class="UnauthorizedLayout-content-preChildren"></div>\n' +
    '            <div class="UnauthorizedLayout-content-children">\n' +
    '                <div class="LoadingSpinner" aria-busy="false">\n' +
    '                    <form class="LoginScreen" action="/logout" method="post">\n' +
    '                        <div class="panel panel-default">\n' +
    '                            <div class="UnauthorizedHeader panel-heading">\n' +
    '                            <h1 class="UnauthorizedLayout-header" style="padding-bottom: 25px;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAm8AAACHCAYAAABAvr5OAAAACXBIWXMAAAsSAAALEgHS3X78AAAUvElEQVR42u3dP08b277G8SecfWtTuGciXV1FlhA+lTtjGgo3OLovAKcJJXZ1S4ZXYFOGJuYVbKdxkSaDO1cxioTo9tBT2NKtL7dYi2zChgAzy/P3+5Gifc7eAXt+s2bmmVlr1npze3sruPfpx1dP0lCSJ6l3sLkbUBUAABDXG8Kb89C2Lqkn6ejBf/piQ1xIlQAAAOEtG8GtK/O0rfLEX1na/z482NxdUDEAAEB4Sye0tWwo23rhj1xL8g82d0dUDwAAEN6SC22eJF/SfsRfcW5DXEA1AQAA4W11oe1uXFtPT3eRvsaZzHg4ulIBAADhzXFw68o8bdtw/KuXMmPhfKoMAAAIb/FDW11mXNv2ij/qWuYp3JiqAwAAwtvrQ9u6DW37CX/0uQ1xc/YCAAAgvL0suPlyN64tqhOZlxoYDwcAAAhvT4S2lqSR3I9ri2ppA9yQvQMAAOGNKvwd2jwb2rYz+hWvJXWZWgQAAMJb2UPbuswbpIc5+costQUAAOGttMGtZ4NbJYdf/1gstQUAAOGtJKGtpdctaZVVS5mncCOaMwAAhLcihjbPhra9gm3ahQ1xAc0aAADCW1GCmy/pqOCbyVJbAAAU2FrJtveoBNu4L6lO0wYAgPAGAAAAwhsAAAAIbwAAAIQ3AAAApOkPSgDXGs12S5Jn/+Bv4Ww6GVEGAADhDVkJbXlerSIJ5zJr5wIAQHhDqqFt3YaSPaoBAADhDdk3lrRNGQAAWD1eWEAstquU4AYAAOENOeFTAgAACG/IAftWKS8nAABAeENOtCgBAACENwAAABDeAAAACG8AAAAoengbXN70Bpc3LcoPAADwOolO0ju4vOlIGkrasP//i6Rev1YN2RUAAAAZCW+Dy5u6DW0PJ3Pdk7Q3uLw5ljTs16oLdgkAAMDTVtptOri8WR9c3gwlfdfvZ+E/khQOLm+67BIAAIAUwtvg8qYnKZR0+MIfqUj6PLi8CRgPBwAA8Djn3aY2eI1kx7VFsC3p2+Dy5kySz3i44ppNJ2+oAgAAKYW3weWNJzOubc/Rr9yX1LHdroyHAwAAkINuUzuuzZf0l8PgdqciMx5ubt9UBQAAILzFCG5dmXFtRyv+nhuS/rTj4ersNgAAUFaRuk3tuLahpK2Ev++2pO92PFyPrlQAAEB4+31o8yT5MuPR0vRzPFy/VvXZjSi7RrPdklSX5Nl/pi2QeSo/n00n85Rq0pXUsjXJovCuRpLCtOrksN7rtt537TDtui/uaispmE0nYc5qWb/Xfj2O6Uh1rN9rj60MHfOBrWPkB1Bvbm9vXxLa1iX17J9KxvbPtaRuv1YNnvuLn358vVU57Bxs7gYJHBi+YnSZ87Zp7Pp3JHXlfqypa0tJY0nj2XQyTuiEPVLyPQMu6hTcq9UiB21w3bbBbg7qfW1rO8pi+Gg0256kTk5qeXdMD7NWS3tevPtTyXgdL2R6MV99vD8b3uy4Nl/Rp/5IyrkNcSHhjfBWgtA2zMEx+dQF1J9NJ6MV1aYr6XNBdvWZDRpBBtvgum2DebhAPnW98LNQWxvafKXfo5XrWtpjPw9Z5akwPLRh+EUh7snw9pslrbLuRGZ+uAXhjfBWsNC2bu92twuwOdeSui5P+PaJ2/cC7vrMBA1b5569SFYKUtteGk+P7PHs6+UT2efi2pv0E2M7ZGSU09D22Hmx95IeirVHQtv64PJmpOeXtMqqQ5mltnpc7lGg4NaSGSuxXZBN2pD0rdFsDx3+zlFBd/+2rdXYPqVJ7eah0WwHkgYFCW53tf1uA2mStazLjMc7LFA7PZQ0t9uWVB2Hkr4VJLjdnRf/fMl5ce1BcPPtBWI/5wWoSBoMLm/mLLWFAgS3rj1BVQq4eYeNZntun0LEqVFH+Rvj9lp79uLYS6EN1mXG420XtLaDRrM9itsOX3E8fy9Q4HgYPgJ7PK76RqJo4ffheTH4XXtcs6GtNbi8CWW6wIp0gdiSWWprbN+UBfIY3D4XfDO37Ak/zoWzLPM/VmzQGCcRNO4ulDLd9UUPx/sO2iHHs2mjf64qwNn9E5SgPW7/rj2u2SdTRXrs+NQd618y4xuK7lrm6SnyH9xaJTjR3w9wY/b6q85pQUIBLij49eFhOxwR3JwYue5CLVFwu98eH+1CXStRQ9LB5m5L0gcbcIpmKen4YHPXO9jcJbzlP7itlzDMbDseA1eGE/uqnxSlMRl76sHYvozlso71py7CBVaxAc5l+yxje9x/bKjEWsmKoIPN3ZFMF8uxDTxFcCbJO9jc9YXC3LWqmGPcnnNonzgi5QBn98NhSet65PipUVmP5y2ZN2pdtMeO8j8ePyr/4ctKa2WswsHm7sIGnboNPnl1LjMtSPdgc5elwgrCXjT3SlwCnr5FCHDsh2xuv32Kt1XiOh7GfUv63ryCZVV5GILXynxkHmzuhgebu11JOzIzHefFtaQPB5u7rSTmc0Pyd1llDyN2fBBeVzNnFzdb/62S13Q7bju0oYNpq+Kf03oqz7jLp+zffxq8RpuSDjZ3g4PN3brMeLgsd6UuZbp767b7FwVjD85tKlH6ABuFyy5nAofRdVDHCmXUftSnbwTgx4/LP6jFLyFu9OnH17Et0FHGvt4XSb2iv4xwb0Hm3Io5Ez4nKWOj0Wy3srg0VMaNFHMRc3sDsUUpJZmnb16MRe27lPCXc1uU81tel2BbVQjuzaaTBeHtnwFuIcn/9OPrSKaPPe2xRxc2tJXlIlaXmbomz+Is++VybqSlzCzuSVl3fNHvaDVjuYoeersx144lcPyzHn7EELxB+X45nqOGN/xajxHh7ekQF0rqfPrxtaV01k1b2tA2Ym+Ugz3Zx73DvJBZX3Cc8nb0FP/NsFaCx9o8hVKtqnvcV7y5yjo5r+v9G0EXT2w6itaN7zJ0XEhK66U0VzdlGxGfYu7RHv9xXiS8vSDEBZK8Tz++JrkY87GkIW+Qlk7csHI2m066aW+EXeS722i2RzJz1UU9ZpLqupvPppNWGrWy0x+4ngIh8tM3Oy7JxY1qX9Io6UXKH9merkwPSpzz9lZKx7Nkh8vE6LZ1VUfP3hDEveFovebGwtEYzqWtYeoPQux8bXFzREfihYXXhLihzFiSkxV+zLmktwebuz7BrZTijPW7yEJwexDiAsXsgiv6nG+z6WRs99tbe6F2JepTH8/BZ+/MppNh2sHN1nfkIkRFbIdxx+6ezaaTTtrBzdYxtDc4cdtoPeH2uJTUykJws3UcOmiPlUaz7RHeXhfgFgebuz17onW51Na1zHxtLVZHKLU4J6pMvuhgu2/P2bUvujh2ZN54d2Ev4tt9cS8sx1l7ycQ+CT5J4aPjPF25ztrNmBX3OyUd3np2/xetPRLeIoa40C619V7xltpaSurbJa0CKlt6Ue/Ulxl/K3OcQk3yGuJGDgNcK4VNyOpEqqMka+lgxYtxRtvnQvGevnkJf+VxQdtjnfAWL8SNDzZ3PUVbautEZkkrZpNH3Dv1eca3K873Wy9bI7ABru/gV0XpOo0T+C6y0FX6RE2TPkbi3nSMC3o8byRYxyK3x3XCm5sQ59s7ipcstXUu6d8Hm7s9xrUBeOLkPlT87uakn1pyPoNr67THxxHe3AW4xb2lth476V5Lem/Htc2pGErE4wQciR/z5zdWsWA9AMJbEUNcYMfDfbCBbSnp2I5rG1MhlFArxs+W9kbHjmPM29M3AAlgnrcVefMf/zWW/u/dG93+563+NaIiKCP7xmOcOczK3hU3Vry5tXjyBhQQT95W4PQq7EoKpbX/udW//lvS/PQq9E+vQk6kKGP4iCxrr/mnIIj58zx5AwhveCa0tU6vwrmkz/r1zcGKzEL389OrkHXaUHiNZttrNNtzxVslofTzwxFeATyGblM3oc3Tyxax35D05+lVeC6p9/Gdx4kZqw5RfgofW5eb9QgD9iAAEN5ch7Z1mZnte3rdHF3bkr6fXoVnNsTxij1W5SjH350XfIwLJbfOKwDCW6GDW1fmVf44izjvS+qcXoXDj+88n6pKMm8X7lCG0rumy/Anbu4AEN5ihraWDW3bjn5lRdKRDYPdj++8oMz1tTNiB7S00mPlEQAgvMUObev2grK/oo/YkPTNjofrfnznhVQdJbVU/LX/AKCweNv0ZcHNlxSuMLjdty3pr9OrcMjUIiipXlbXJAQAwlv2Q1vn9CoMZQZ9VxL++ENJ4elV2GNPoETO7cLsAIAn0G36eGiry3SRbqf8VSqSBnY8XK/s4+FQeEtJXcoAAIS314S2dZmXEQ4z9tW2ZMbDfbEhLmRvoYA6s+mEtg0Az6Db9O/g1pMZ13aY4a+5JzMejqW2UDQf7ELsAADC27OhrWXHtQ2U/Li2qO6W2urShJFzS0k7jHMDAMLbS0Kbd3oVjiV9U7yJdtOyIenz6VUY2LnngLy5kFTniRsAvE7pxrzdW9LqqCCbtC0zHu5Mks94OOTAUpI/m06YiBcAIijVkzfbzRgWKLjdty/TlerTrJHh0HYsySO4AUB0f/Rr1WBwefNeZmqMjYJu5xeZp21/FXx/3i21FTCtCDLiXGa92mA2nbDQPAC4CG+S1K9Vx5LGg8sb34acSkG271pSt1+rBpJ0evW/7HGUzU4aH8o4NgBYcXi7069V/cHlzVCrXcMzCUtJfr9WpWsGpUaIQoI8SkAtHQsVfbL8QtfwH2Pe+rXqol+rdu0d+3kOt+lEkkdwQ4nUKQEciRP2NxrNNqHDzU1TK8Ob14nxs8sI4a1w7bHRbHfi/o4nX1jo16pBv1ZtSfog0/2YdeeS3vZr1V6/VmVRa+RR1JulSqPZ7lI+ZADt0FFAajTbmZuIvdFst2RW/IlqnvBX7hX0OAmefdu0X6uO7J39cYTUnIRrSe/7tWqrX6uGHPMoqWEWT/bInbgX1yNuJH66iPGzFUlBlo5p+13i9mgtEm6Ph1lrj/b77MWt44umCrFdqb4NcV8yUoOlpON+rerZFy6AvAtinuznjWabLlTE4eIG+HOj2R7RFmPXcsse0900Q1yj2fZs4AgV76lblDBWmPbYaLZbjWZ7LOlz3N81m07mr5qk1z7Z6gwub1o2gW+lVIczSXSPomji3mVuSPreaLa/SBo7OvG96kTLwvL5NptO5o1me6n4Mw7sS9pvNNtpbMZORl7UCRT/CcuGvdh/TqmWro1L2B5dO5cirrBgp96oDy5vujbEVRL80v7d1B9Awbhq13sOLhpRHEvy2Y2FuMDuUwYndRxQhp+Ws+lkTnt0E4BjrbBgx8N5Mm94rtK1pA92XBvBDYU0m04Wys6wBJTXiBI4OZ5DxRv3Rru6F1bgMLzZALfo16o9SW/lfmqRu+V06jYoAkXHFDdIO3QEyscMAxzPJaiFXZmF9mic3Q1Ncba2ab9WDe3UIjuOCv3FhjafsW0o2YXznEogZT4lcHI8jwgekqTzmONhaY/G6O5/OF+Y3s4P50nqK9rUIheSdvq1aoepP8CFEyB05FyXEsSrgW2PZb+p/XL/RZy1VX2KXeHAk3kz9CWWMuPa6oxrQ8kvnIFWP44UIHQkdzyXeSzrsaO30HvK5lyzSVjqwYTDa6v8tHtLbf37mdR8LLOk1YhDHZBknr4x2Blph45jKuEsCJfxSebFbDrxHbXHubK7YsLK28/DALyWxKf2a9W5HQ/3/kEDvlvSinFtwK8nqoU94S+pBlJsh75e3nuC3x/PnZIdz0s5XqPVdp+WrT2e2Jc2lHh4uxfixvfGw+2wpBXw7J1miwCHlNthlwDH8RwluNnQSnuM7mw2nTz6tHEtjW/Tr1WHjGsDXnXCZ/A4CHAcz3lwIcmLOCHva9pj0ccF9+12KjPhDcCrT/hZWlcY5Q1w78WTYI7np53MppP6Kp64PVLHXkHb47XMEm+/nReP8Abk44S/mE0nHf1z3CiQZDsc63WzCKAcx/O5DRw92mNkPxcleMnavIQ3IGcXz9l04kn6QIhDisGjK7Oqzpl4Elfm4/kutLVeEjgSao95DW3ebDrxX/rU8g8OHyCXJ/2RpFGj2a7LvJXakrRFZZBgGwwldRvN9rrMm5Qd2w4rVCfW8XxXy6wez+cy62uOHc3f5ro99u61xZakjQzW8FpSYGsYae1WwhviCChB6iesX+Y+ajTbLZluBK9k7SHOZ4cZ382jGNsXJNAGF/Y7jmwbvGt/rZTqFeb8eJ7LrrKS8vF831xSuMqXEBJoj3VJ6yl+tYWt49zFmMA3t7e3pbnQnV6FZdnYnY/vPIIVAPx9YxPn/H/sarJZwAXGvAEAABDeAAAAQHgDAAAgvAEAAIDwBgAAAMIbAAAA4Q0AgIKyk98ChDcAAHKiG/PnA0oIwhsAAAm4t4QcQHjLqTIs5L2UWYYDAMoe3DoyT81irbea1qLrwFPKtrZpXWYdyJ6KuXjymaTex3ce4Q1AVgJUS8mvc+rJ3aLkF+xFEN5SZEONf3oVjiQNJe0VZNPObWib06QBZExL0lGOv3/ALgThLRshLpTUOb0KWzbEbeV0U65taBvTlAFgJUaUAFlT6hcWPr7zgo/vvLqkDzJjxfJiKelYUp3gBgArczGbTujRAOEtoyFuJDNG4iQHX/fMhjafsW0AsFJDSoAs+oMS/AxwC0m906twKPOYfDtrd4AyXaQBewsAVu56Np2MKAMIb/kIcaGklh0PN5Kbt5XiWNrQxkkEAJLTpQTIKrpNnw5xwcd3niepr/TGwx1L8ghuAJCoE+Z2A+Et3yFuKDMe7izBj/0i6S3j2gAgcRez6aRHGZBldJu+LMAtJHXteLihVjce7lpSl3FtAJBOcFPyEwoDhLcVh7i5zHi4jg1xrsbDLSX59ikfACCl4DabTujtQObRbRotxI3teLhjxR8PdyIzro3gBgDpOJlNJ3WCGwhv5Qhxvsx6qVHGw53LjGtjLVIASMe1pB3GuCFv6DaNH+BCmfFwI0m+nh8Px5JWAJB+aPOZxw2EN0JcIDMeriszHq7y4K8sJQ3t0zoAQLKWksaSxrPphJtnEN7wS4gbnV6FY0k9SUf2X5/JPG2jexQAVu9C0kJSaP8EzNuGInlze3tLFVbk9Cr0bKALqQYAAHDh/wG6uYldf87TEgAAAABJRU5ErkJggg==" class="AppLogo" alt="Fengoo"></h1>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col-xs-6"><h2>Odhlášení</h2></div>\n' +
    '                                    <div class="col-xs-6 md-hidden sf-hidden"></div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="panel-body LoginScreen-body">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col-md-12">\n' +
    '                                        <div class="col-md-12"><p>Opravdu si přejete se odhlásit?</p></div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="panel-footer">\n' +
    '                                <div class="row">\n' +
    '                                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left"><span class="ButtonWrapper"><span class="ButtonWrapper-container"><button id="cancelLogoutBtn" type="button" class="react-boostrap-button-wrapper btn btn-primary"><div>Zrušit</div></button></span></span></div>\n' +
    '                                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right"><span class="ButtonWrapper"><span class="ButtonWrapper-container"><button type="submit" class="react-boostrap-button-wrapper btn btn-danger" style="background-color: brown;border-color: red;"><div>Odhlásit se</div></button></span></span></div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="FormCopyright sf-hidden"></div>\n' +
    '                        </div>\n' +
    '                    </form>\n' +
    '                </div>\n' +
    '            <span></span></div>\n' +
    '       </div>\n' +
    '           <div class="UnauthorizedLayout-content-postChildren"></div>\n' +
    '       </div>\n' +
    '   </div>'
