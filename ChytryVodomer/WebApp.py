#!/usr/bin/python3
"""WebApp.py: webova aplikace pro spravu vodomeru a ventilu"""
__author__      = "Ladislav Hlousek"
__copyright__   = "Copyright 2018, Ladislav Hlousek"
__version__     = "1.0.0"
__maintainer__  = "Ladislav Hlousek"
__email__       = "ladishlousek@gmail.com"
__status__      = "Production"

from flask import Flask, render_template, request, redirect, url_for, flash,json,jsonify, send_file, send_from_directory
from flask_compress import Compress
from datetime import datetime
import psutil
from passlib.context import CryptContext
from flask_httpauth import HTTPBasicAuth
from flask_mail import Mail, Message
from zipfile import ZipFile
import sqlite3
import os,sys,signal,subprocess
import json
import base64
from crontab import CronTab
from hostapdconf.parser import HostapdConf

# Nejsnažší řešení problému, nutno vyřešit rychle
try:
	from flask_login import LoginManager, login_required, current_user, login_user, logout_user, UserMixin
except:
	os.system('pip3 install flask-login')
	from flask_login import LoginManager, login_required, current_user, login_user, logout_user, UserMixin
	pass

class User(UserMixin):
	def __init__(self, id, login, heslo, ulozeno, authenticated = False, active = False, anonymous = False):
		self.id = id
		self.login = login
		self.heslo = heslo
		self.ulozeno = ulozeno
		self.authenticated = authenticated
		self.active = active
		self.anonymous = anonymous
		pass

	def is_authenticated(self):
		return self.authenticated

	def is_active(self):
		return self.active

	def is_anonymous(self):
		return self.anonymous

	def get_id(self):
		return str(self.login)

	pass

pwd_context = CryptContext(
	schemes=["pbkdf2_sha256"],
	default="pbkdf2_sha256",
	pbkdf2_sha256__default_rounds=30000)

db = "/home/pi/ChytryVodomer/db/chytryvodomer.db"

writer = sqlite3.connect(db, isolation_level=None, check_same_thread=False)
writer.execute('''PRAGMA journal_mode=WAL;''')
#writer.commit()

reader = sqlite3.connect(db, isolation_level=None, check_same_thread=False)
reader.execute('''PRAGMA journal_mode=WAL;''')
#reader.commit()

writer.row_factory = lambda c, r: dict(zip([col[0] for col in c.description], r))
reader.row_factory = lambda c, r: dict(zip([col[0] for col in c.description], r))

apconf = HostapdConf('/etc/hostapd/hostapd.conf')

def getLocalIPs():
	eth_ip = subprocess.Popen(['ifconfig eth0'], stdout=subprocess.PIPE, shell=True)
	(EIP,eth_errors) = eth_ip.communicate()
	eth_ip.stdout.close()
	
	wlan_ip = subprocess.Popen(['ifconfig wlan0'], stdout=subprocess.PIPE, shell=True)
	(WIP,w_errors) = wlan_ip.communicate()
	wlan_ip.stdout.close()
	
	ip_eth_str = EIP.split(b'inet ')
	ip_wlan0_str = WIP.split(b'inet ')	
	
	if len(ip_eth_str) <= 1:
		ip_eth_str = "-"
	else:
		ip_eth_str = ip_eth_str[1].split(b' ')[0].decode()
	
	if len(ip_wlan0_str) <= 1:
		ip_wlan0_str = "-"
	else:
		ip_wlan0_str = ip_wlan0_str[1].split(b' ')[0].decode()
	
	return [ip_eth_str, ip_wlan0_str]

def change_static_ip(ip_address, routers, dns):
	conf_file = '/etc/dhcpcd.conf'
	try:
		with open(conf_file, 'r') as file:
			data = file.readlines()
		
		ethFound = next((x for x in data if 'interface eth0' in x), None)
		if ethFound:
			ethIndex = data.index(ethFound)
			if len(ip_address) > 0:
				if data[ethIndex].startswith('#'):
					data[ethIndex] = "interface eth0\n"
					data[ethIndex+1] = "static ip_address="+ip_address+"/24"+"\n"
					data[ethIndex+2] = "static routers="+routers+"\n"
					data[ethIndex+3] = "static domain_name_servers="+dns+"\n"
			else:
				if ethIndex:
					data[ethIndex] = "#interface eth0\n"
					data[ethIndex+1] = "#static ip_address="+"\n"
					data[ethIndex+2] = "#static routers="+"\n"
					data[ethIndex+3] = "#static domain_name_servers="+"\n"

			with open(conf_file, 'w') as file:
				file.writelines(data)
			
			os.system("sudo dhcpcd -k eth0")
			os.system("sudo dhcpcd -p eth0")
	except Exception as ex:
		print("IP changing error: " + ex)
		pass
	finally:
		pass


def write_log(log_type, log_title, log_data):
	file_name = "/home/pi/ChytryVodomer/logs/webapp.log"
	lines = []
	if os.path.exists(file_name) == False:
		open(file_name, "x")
	else:
		with open(file_name, "r") as file:
			lines = file.read()
	
	now = datetime.now()
	dt_string = now.strftime("%d.%m.%Y %H:%M:%S.%f")
	# dd.mm.YY H:M:S.milisec
	
	with open(file_name, "w") as f:
		if(len(lines) > 10000):
			lines = lines[:-1]
		if(len(lines) > 0):
			f.writelines(lines)
		
		f.write(log_type + ";" + dt_string + ";" + log_title + ";" + log_data + "\n")
	
def signal_handler(signal, frame):	
	write_log("WARNING", "Application", "Closed by user input Ctrl+C")
	
	print('Ctrl+C, exiting')
	sys.exit(0)
	
signal.signal(signal.SIGINT, signal_handler)


APP_ROOT = os.path.dirname(os.path.abspath(__file__))
os.path.join(APP_ROOT, 'logs')

sys.path.append('static')

app = Flask(__name__, static_folder='static')
app.config["COMPRESS_REGISTER"] = False
app.config['SECRET_KEY'] = "AyQsdwqs!@"
compress = Compress()
compress.init_app(app)
auth = HTTPBasicAuth()
cron = CronTab(user='root')
login = LoginManager()
login.init_app(app)
login.login_view = "/login"


def enc_pwd(password):
	return pwd_context.hash(password)


def check_pwd(password, hashed):
	return pwd_context.verify(password, hashed)


cur = reader.cursor()
cur.execute('''SELECT * FROM nastaveni_email ORDER BY Ulozeno DESC LIMIT 1''')
nasEmail = cur.fetchone()

cur.execute('''SELECT * FROM uzivatele ORDER BY Ulozeno DESC LIMIT 1''')
uzivatelData = cur.fetchone()

cur.close()

def measure_temp():
	temp = os.popen("vcgencmd measure_temp").readline()
	temp = temp.replace("temp=","")
	temp = temp.replace("'C","")
	return temp
	
def measure_core_volts():
	volts = os.popen("vcgencmd measure_volts core").readline()
	return (volts.replace("volt=",""))

def measure_cpu():
	return str(psutil.cpu_percent()) + '%'

def measure_memory():
	memory = psutil.virtual_memory()
	# Divide from Bytes -> KB -> MB
	available = round(memory.available/1024.0/1024.0,1)
	total = round(memory.total/1024.0/1024.0,1)
	return str(available) + 'MB free / ' + str(total) + 'MB total ( ' + str(memory.percent) + '% )'

if nasEmail is not None:
	if "ID" in nasEmail:
		app.config['MAIL_SERVER'] = nasEmail['Server']
		if nasEmail['Port']:
			app.config['MAIL_PORT'] = nasEmail['Port']
		else:
			app.config['MAIL_PORT'] = 465

		app.config['MAIL_USERNAME'] = nasEmail['Username']
		app.config['MAIL_PASSWORD'] = nasEmail['Pass']

		if nasEmail['Security'] == "SSL":
			app.config['MAIL_USE_TLS'] = False
			app.config['MAIL_USE_SSL'] = True
		if nasEmail['Security'] == "TLS":
			app.config['MAIL_USE_TLS'] = True
			app.config['MAIL_USE_SSL'] = False
		if nasEmail['Security'] == "none":
			app.config['MAIL_USE_TLS'] = False
			app.config['MAIL_USE_SSL'] = False

mail = Mail(app)

def SendMail(sub,body,html=""):
	res = False
	try:
		cur = reader.cursor()
		cur.execute('''SELECT * FROM nastaveni_global ORDER BY Ulozeno DESC LIMIT 1''')
		globalNastaveni = cur.fetchone()
		cur.close()
		if "ID" in globalNastaveni:
			if globalNastaveni['Email'] != "":
				global nasEmail 
				if nasEmail['FromEmail'] != "":
					if body != "" or html != "":
						if sub == "":
							sub = "Bez předmětu"
						msg = Message(subject = sub , body = body, html = html, sender = nasEmail['FromEmail'], recipients = [globalNastaveni['Email']], charset="UTF-8")
						mail.send(msg)
						res = True
					else:
						res = "body"
				else:
					res = "from"
			else:
				res = "email"
		else:
			res = "globalNastaveni"
		conn.close()
	except Exception as e:
		#print("SendMail() - Chyba!")
		#print(e)
		res = e
		write_log("INFO", "MAIL", "E: {0}".format(res))
	return res

def openVentil():
	try:
		now = datetime.now()
		ts = datetime.timestamp(now)
		writer.execute('''BEGIN''')
		writer.execute('''INSERT INTO ventil_data (StavVentil, AutoAkce) VALUES (1,0)''')
		writer.commit()
		write_log("INFO", "Ventil", "state: open")
	except:
		pass

def closeVentil():
	try:
		now = datetime.now()
		ts = datetime.timestamp(now)
		writer.execute('''BEGIN''')
		writer.execute('''INSERT INTO ventil_data (StavVentil, AutoAkce) VALUES (0,0)''')
		writer.commit()
		write_log("INFO", "Ventil", "state: closed")
	except:
		pass


def createZip(file_name):
	with ZipFile(file_name, 'w') as zf:
		if os.path.exists('/home/pi/ChytryVodomer/logs/program.log') == True:
			zf.write('/home/pi/ChytryVodomer/logs/program.log')
			
		if os.path.exists('/home/pi/ChytryVodomer/logs/ventil.log') == True:
			zf.write('/home/pi/ChytryVodomer/logs/ventil.log')
		
		if os.path.exists('/home/pi/ChytryVodomer/logs/webapp.log') == True:
			zf.write('/home/pi/ChytryVodomer/logs/webapp.log')
		
		if os.path.exists('/home/pi/ChytryVodomer/logs/wsagent.log') == True:
			zf.write('/home/pi/ChytryVodomer/logs/wsagent.log')
			
		if os.path.exists('/home/pi/ChytryVodomer/logs/tmplog.log') == True:
			zf.write('/home/pi/ChytryVodomer/logs/tmplog.log')
	return True


@auth.verify_password
def verify_pw(username, password):
	try:
		cur = reader.cursor()
		cur.execute('''SELECT * FROM uzivatele ORDER BY Ulozeno DESC LIMIT 1''')
		data = cur.fetchone()
		cur.close()
		if username == data["Login"] and check_pwd(password, data["Heslo"]):
			return True
		else:
			return False
	except:
		pass
	return False


@app.route('/logout',methods=["get","post"])
def logout():
	logout_user()
	return redirect('/login')

@app.route('/version')
def getVersion():
	ver = ""
	update = False
	newVer = ""
	with open('/home/pi/info/info.json',) as f:
		infoData = json.load(f)
		ver = infoData["Version"]
		if infoData["NeedUpdate"] != update:
			update = infoData["NeedUpdate"]
			newVer = infoData["NewVersion"]
	if update == True:
		flash("<b>POZOR!</b><p>Aktuální verze systému je zastaralá. Je dostupná aktualizace na novou verzi <b>" + newVer + "</b>. Přejděte do <a href=\"/nastaveni\">Nastavení</a> a proveďte aktualizaci.</p>" , "warning")
		
	response = app.response_class(
		response=json.dumps(ver),
		mimetype='application/json'
	)
	return response


def writeWSKey(wskey):
	return wsKey


@app.route('/')
@compress.compressed()
@login_required
def index():
	pocetL = 0
	prutok = 0
	limitL = 0
	celkemL = 0
	stavVentil = 0
	limitJed = "L"
	tel = ""
	telHeslo = ""
	data = []
	start_mereni = datetime.now()
	try:
		cur = reader.cursor()
		cur.execute('''SELECT * FROM vodomer_data ORDER BY TsTime DESC LIMIT 1''')
		akt_data = cur.fetchone()
		
		cur.execute('''SELECT * FROM vodomer_data ORDER BY TsTime DESC LIMIT 10''')
		_data = cur.fetchall()
		
		cur.execute('''SELECT * FROM ventil_data ORDER BY TsTime DESC LIMIT 10''')
		_ventilData = cur.fetchall()
		
		cur.execute('''SELECT * FROM ventil_data WHERE StavVentil = 1 ORDER BY Ulozeno ASC LIMIT 1''')
		ventilStart = cur.fetchone()
		
		cur.execute('''SELECT * FROM ventil_data ORDER BY TsTime DESC LIMIT 1''')
		ventilData = cur.fetchone()
		
		cur.execute('''SELECT * FROM nastaveni_global ORDER BY Ulozeno DESC LIMIT 1''')
		globalNastaveni = cur.fetchone()
		
		if akt_data is not None:
			if "ID" in akt_data:
				pocetL = akt_data['PocetLitry'] if akt_data['PocetLitry'] else 0
				celkemL = akt_data['CelkemLitry'] if akt_data['CelkemLitry'] else 0
				prutok = akt_data['Prutok'] if akt_data['Prutok'] else 0
				
		
		format = "%Y-%m-%d %H:%M:%S"
		formatOut = "%Y-%m-%d"
		if ventilStart is not None:
			if "ID" in ventilStart:
				start_mereni = datetime.strptime(ventilStart["Ulozeno"], format)

		if ventilData is not None:
			if "ID" in ventilData:
				stavVentil = ventilData['StavVentil']
		
		if globalNastaveni is not None:
			if "ID" in globalNastaveni:	
				limitL = globalNastaveni['LimitLitry']
				limitJed = globalNastaveni['LimitJednotka']
				if globalNastaveni['PocatecniStavLitry'] > 0:
					celkemL = celkemL + globalNastaveni['PocatecniStavLitry']
		
	except Exception as e:
		pass
	
	cur.close()
	
	dictx = {
		'POCET_LITRY': pocetL,
		'LIMIT_LITRY': limitL,
		'LIMIT_JEDNOTKA': limitJed,
		'CELKEM_LITRY': celkemL,
		'STAV_VENTIL': stavVentil,
		'PRUTOK': prutok,
		'TEL': tel,
		'TEL_HESLO': telHeslo,
		'START_MERENI': start_mereni.strftime(formatOut)
	}
	
	return render_template('index.html', result = dictx, vodData = _data, venData = _ventilData)


@app.route('/kalendar')
@compress.compressed()
@login_required
def kalendar():	
	return render_template('kalendar.html')
	
@app.route('/stats')
@compress.compressed()
@login_required
def stats():
	start_mereni = datetime.now()
	teplota = ""
	teplota = measure_temp()
	napeti = ""
	napeti = measure_core_volts()
	cpu = "0%"
	cpu = measure_cpu()
	
	ips = getLocalIPs()
	print("IPS", ips)
	
	mem = ""
	mem = measure_memory()
	
	cur = reader.cursor()
	cur.execute('''SELECT * FROM ventil_data WHERE StavVentil = 1 ORDER BY Ulozeno ASC LIMIT 1''')
	ventilStart = cur.fetchone()
	format = "%Y-%m-%d %H:%M:%S"
	formatOut = "%Y-%m-%d"
	if ventilStart is not None:
		if "ID" in ventilStart:
			start_mereni = datetime.strptime(ventilStart["Ulozeno"], format)
			
	
	cur.close()
	data = {
		'START_MERENI': start_mereni.strftime(formatOut),
		'TEPLOTA': teplota,
		'NAPETI': napeti,
		'CPU': cpu,
		'MEM': mem,
		'ETH_IP': ips[0],
		'WIFI_IP': ips[1]
	}
	return render_template('stats.html', result = data)

@app.route('/getChartData',methods=["POST"])
@compress.compressed()
@login_required
def getChartData():
	actDate = datetime.now()
	endDate = datetime.now()
	start =  actDate.strftime("%Y-%m-%d 00:00")
	end = endDate.strftime("%Y-%m-%d %H:%M")
	dataj = request.get_json()
	
	if dataj is not None:
		if len(dataj["start"]) > 1:
			start = dataj["start"]
			
		if len(dataj["end"]) > 1:
			end = dataj["end"]
			
		print(dataj)
	
	vodData = {}
	vodDataMinLabels = []
	vodDataMinValues = []
	
	vodDataHLabels = []
	vodDataHValues = []
	
	vodDataDLabels = []
	vodDataDValues = []
	
	vodDataMLabels = []
	vodDataMValues = []
	
	cur = reader.cursor()
	
	cur.execute('''SELECT COUNT(PocetLitry) as Pocet, strftime('%Y-%m-%d %H:%M', Ulozeno) as Ulozeno FROM vodomer_data WHERE PocetLitry != 0 AND Ulozeno >= ? AND Ulozeno <= ? GROUP BY strftime('%Y-%m-%d %H:%M', Ulozeno) ORDER BY ID ASC''', (start,end))
	vodDataMin = cur.fetchall()
	for itm in vodDataMin:
		vodDataMinLabels.append(itm["Ulozeno"])
		vodDataMinValues.append(itm["Pocet"])

	cur.execute('''SELECT COUNT(PocetLitry) as Pocet, strftime('%Y-%m-%d %H:00', Ulozeno) as Ulozeno FROM vodomer_data WHERE PocetLitry != 0 AND Ulozeno >= ? AND Ulozeno <= ? GROUP BY strftime('%Y-%m-%d %H', Ulozeno) ORDER BY ID ASC''', (start,end))
	vodDataH = cur.fetchall()
	for itm in vodDataH:
		vodDataHLabels.append(itm["Ulozeno"])
		vodDataHValues.append(itm["Pocet"])
	
	cur.execute('''SELECT COUNT(PocetLitry) as Pocet, strftime('%Y-%m-%d', Ulozeno) as Ulozeno FROM vodomer_data WHERE PocetLitry != 0 AND Ulozeno >= ? AND Ulozeno <= ? GROUP BY strftime('%Y-%m-%d', Ulozeno) ORDER BY ID ASC ''', (start,end))
	vodDataD = cur.fetchall()
	for itm in vodDataD:
		vodDataDLabels.append(itm["Ulozeno"])
		vodDataDValues.append(itm["Pocet"])
		
	cur.execute('''SELECT COUNT(PocetLitry) as Pocet, strftime('%Y-%m-%d', Ulozeno) as Ulozeno FROM vodomer_data WHERE PocetLitry != 0 AND Ulozeno >= ? AND Ulozeno <= ? GROUP BY strftime('%Y-%m', Ulozeno) ORDER BY ID ASC''', (start,end))
	vodDataM = cur.fetchall()
	for itm in vodDataM:
		vodDataMLabels.append(itm["Ulozeno"])
		vodDataMValues.append(itm["Pocet"])
	
	cur.close()
	
	vodData = { 
			"minute": { "labels": vodDataMinLabels, "data": vodDataMinValues }, 
			"hour": { "labels": vodDataHLabels, "data": vodDataHValues }, 
			"day": { "labels": vodDataDLabels, "data": vodDataDValues }, 
			"month" : { "labels": vodDataMLabels, "data": vodDataMValues } 
	}
	print(json.dumps(vodData))
	return jsonify({'payload':json.dumps(vodData)})
	


@app.route('/saveEvent',methods=["POST"])
@login_required
def saveEvent():
	dataj = request.get_json()
	print(dataj)
	
	stavVentil = "0"
	
	id = None
	if 'id' in dataj:
		id = dataj["id"]
		cur = reader.cursor()
		cur.execute('''SELECT * FROM events WHERE ID = ?''', (id,))
		eDb = cur.fetchone()
		stavVentil = eDb["StavVentil"]
		cur.close()
	limitJed = 0
	limiVal = 0
	if dataj["eventLimitJed"] != "":
		limitJed = dataj["eventLimitJed"]
	if dataj["eventLimit"] != "":
		limiVal = dataj["eventLimit"]
	data = {
		'ID': id,
		'Title': dataj["title"],
		'Start': dataj["start"],
		'End': dataj["end"],
		'StartAkce': dataj["eventStartAkce"],
		'KonecAkce': dataj["eventKonecAkce"],
		'LimitJednotka': limitJed,
		'LimitVal': limiVal,
		'Repeat': dataj["eventRepeat"]
	}
	
	format = "%Y-%m-%dT%H:%M:%SZ"
	start_dt = datetime.strptime(dataj["start"], format)
	end_dt = datetime.strptime(dataj["end"], format)
	wrc = writer.cursor()
	update = False
	if data["ID"] != None:
		wrc.execute('''UPDATE events SET Title = ?, Start = ?, End = ?, StartAkce = ?, KonecAkce = ?, LimitJednotka = ?, LimitVal = ?, Repeat = ?, StavVentil = 0 WHERE ID = ?''',(data["Title"], data["Start"], data["End"], data["StartAkce"], data["KonecAkce"], data["LimitJednotka"], data["LimitVal"], data["Repeat"], data["ID"]))
		update = True
	else:
		update = False
		wrc.execute('''INSERT INTO events (Title, Start, End, StartAkce, KonecAkce, LimitJednotka, LimitVal, Repeat) VALUES (?,?,?,?,?,?,?,?)''',(data["Title"], data["Start"], data["End"], data["StartAkce"], data["KonecAkce"], data["LimitJednotka"], data["LimitVal"], data["Repeat"]))
	
	id = wrc.lastrowid
	data["ID"] = id
	
	jobStart = None
	jobEnd = None
	
	repeat = data["Repeat"]

	if update == True:
		cur = reader.cursor()
		cur.execute('''SELECT * FROM events WHERE Title = ? AND Start = ? AND End = ?''', (data["Title"], data["Start"], data["End"]))
		eDb = cur.fetchone()
		id = eDb["ID"]
		cur.close()
		jobStart = cron.new(command="sudo python3 /home/pi/ChytryVodomer/events.py -s " + str(id) + " >> /home/pi/ChytryVodomer/logs/events.log 2>&1")
		jobEnd = cron.new(command="sudo python3 /home/pi/ChytryVodomer/events.py -e " + str(id) + " >> /home/pi/ChytryVodomer/logs/events.log 2>&1")
	else:
		jobStartF = cron.find_command("sudo python3 /home/pi/ChytryVodomer/events.py -s " + str(id) + " >> /home/pi/ChytryVodomer/logs/events.log 2>&1")
		jobEndF = cron.find_command("sudo python3 /home/pi/ChytryVodomer/events.py -e " + str(id) + " >> /home/pi/ChytryVodomer/logs/events.log 2>&1")
		cron.remove(jobStartF)
		cron.remove(jobEndF)
		jobStart = cron.new(command="sudo python3 /home/pi/ChytryVodomer/events.py -s " + str(id) + " >> /home/pi/ChytryVodomer/logs/events.log 2>&1")
		jobEnd = cron.new(command="sudo python3 /home/pi/ChytryVodomer/events.py -e " + str(id) + " >> /home/pi/ChytryVodomer/logs/events.log 2>&1")
	
	if repeat != "none":
		start_rep_cmd = ""
		end_rep_cmd = ""
		
		if repeat == "everyD":
			start_rep_cmd = str(start_dt.minute) + " " + str(start_dt.hour) + " * * *"
			end_rep_cmd = str(end_dt.minute) + " " + str(end_dt.hour) + " * * *"
			
		if repeat == "everyM":
			start_rep_cmd = str(start_dt.minute) + " " + str(start_dt.hour) + " " + str(start_dt.day) + " * *"
			end_rep_cmd = str(end_dt.minute) + " " + str(end_dt.hour) + " " + str(end_dt.day) + " * *"
			
		if repeat == "everyY":
			start_rep_cmd = str(start_dt.minute) + " " + str(start_dt.hour) + " " + str(start_dt.day) + " " + str(start_dt.month) + " *"
			end_rep_cmd = str(end_dt.minute) + " " + str(end_dt.hour) + " " + str(end_dt.day) + " " + str(end_dt.month) + " *"

		if repeat == "everyW":
			dow = start_dt.weekday()
			start_rep_cmd = str(start_dt.minute) + " " + str(start_dt.hour) + " * * " + str(dow)
			end_rep_cmd = str(end_dt.minute) + " " + str(end_dt.hour) + " * * " + str(dow)
			
			if start_dt.minute == 0 and end_dt.minute == 0:
				if start_dt.minute == 0 and end_dt.minute == 0:
					start_rep_cmd = str(start_dt.minute) + " " + str(start_dt.hour) + " * * " + str(dow)
					end_rep_cmd = "59 23 * * " + str(dow)

		if repeat == "everyWD":
			start_rep_cmd = str(start_dt.minute) + " " + str(start_dt.hour) + " * * 1-5"
			end_rep_cmd = str(end_dt.minute) + " " + str(end_dt.hour) + " * * 1-5"
			
			if start_dt.minute == 0 and end_dt.minute == 0:
				if start_dt.minute == 0 and end_dt.minute == 0:
					start_rep_cmd = str(start_dt.minute) + " " + str(start_dt.hour) + " * * " + str(dow)
					end_rep_cmd = "59 23 * * " + str(dow)

		if start_rep_cmd != "" and end_rep_cmd != "":
			jobStart.setall(start_rep_cmd)
			jobEnd.setall(end_rep_cmd)

		print(jobStart)
		print(jobEnd)
	else:
		jobStart.setall(start_dt)
		jobEnd.setall(end_dt)
	
	cron.write()
	
	response = app.response_class(
		response=json.dumps(data),
		mimetype='application/json'
	)
	return response


@app.route('/loadEvents',methods=["GET"])
@login_required
def loadEvents():
	events = []
	db_events = []
	cur = reader.cursor()
	cur.execute('''SELECT * FROM events ORDER BY Ulozeno DESC''')
	db_events = cur.fetchall()
	for row in db_events:
		event = {
			'id': row["ID"],
			'title': row["Title"],
			'allDay': str(False),
			'start': str(row["Start"]),
			'end': str(row["End"]),
			'eventStartAkce': row["StartAkce"],
			'eventKonecAkce': row["KonecAkce"],
			'eventLimitJed': row["LimitJednotka"],
			'eventLimit': row["LimitVal"],
			'stavVentil': row["StavVentil"],
			'isNew': False,
			'eventRepeat': row["Repeat"]
		}
		events.append(event)
	
	response = app.response_class(
		response=json.dumps(events),
		mimetype='application/json'
	)
	return response


@app.route('/removeEvent', methods=["POST"])
@login_required
def removeEvent():
	dataj = request.get_json()
	res = {'msg': "NO"}
	#print(dataj)
	id = "none"
	if 'id' in dataj:
		id = dataj["id"]
		writer.execute('''DELETE FROM events WHERE ID = ?''',(id,))
		writer.commit()
		res = {'msg': "OK"}
		startJob = cron.find_command("sudo python3 /home/pi/ChytryVodomer/events.py -s " + str(id))
		endJob = cron.find_command("sudo python3 /home/pi/ChytryVodomer/events.py -e " + str(id))
		cron.remove(startJob)
		cron.remove(endJob)
		cron.write()
	response = app.response_class(
		response=json.dumps(res),
		mimetype='application/json'
	)
	return response


@app.route('/historie')
@compress.compressed()
@login_required
def historie():
	vodDataHistory = {}
	venDataHistory = {}
	try:
		cur = reader.cursor()
		cur.execute('''SELECT * FROM vodomer_data ORDER BY TsTime DESC LIMIT 2000''')
		vodDataHistory = cur.fetchall()
		
		cur.execute('''SELECT * FROM ventil_data ORDER BY TsTime DESC LIMIT 2000''')
		venDataHistory = cur.fetchall()
	except Exception as e:
		pass

	return render_template('historie.html', vodData = vodDataHistory, venData = venDataHistory)


@app.route('/nastaveni', methods=["GET"])
@compress.compressed()
@login_required
def nastaveni():
	nastaveniVentil = {}
	nastaveniVodomer = {}
	globalNastaveni = {}
	nasEmail = {}
	conf_file = '/etc/dhcpcd.conf'
	netEth = {
		"ip":"",
		"subnet":"",
		"gateway":""
	}
	try:
		with open(conf_file, 'r') as file:
			data = file.readlines()
		
		ethFound = next((x for x in data if 'interface eth0' in x), None)
		if ethFound:
			ethIndex = data.index(ethFound)
			if ethIndex and data[ethIndex].startswith("#") == False:
				ipSub = data[ethIndex+1].replace("static ip_address=","").split("/")
				print(ipSub)
				gw = data[ethIndex+2].replace("static routers=","")
				netEth["ip"] = ipSub[0]
				ipSub = ipSub[1].replace("\n","")
				if ipSub == "8":
					netEth["subnet"] = "255.0.0.0"
				if ipSub == "16":
					netEth["subnet"] = "255.255.0.0"
				if ipSub == "24":
					netEth["subnet"] = "255.255.255.0"
				if ipSub == "32":
					netEth["subnet"] = "255.255.255.255"
				
				netEth["gateway"] = gw
		cur = reader.cursor()
		cur.execute('''SELECT * FROM nastaveni_global ORDER BY Ulozeno DESC LIMIT 1''')
		globalNastaveni = cur.fetchone()
		
		cur.execute('''SELECT * FROM vodomer_nastaveni ORDER BY Ulozeno DESC LIMIT 1''')
		nastaveniVodomer = cur.fetchone()
		
		cur.execute('''SELECT * FROM ventil_nastaveni ORDER BY Ulozeno DESC LIMIT 1''')
		nastaveniVentil = cur.fetchone()
		
		cur.execute('''SELECT * FROM nastaveni_email ORDER BY Ulozeno DESC LIMIT 1''')
		nasEmail = cur.fetchone()
		
		#print(globalNastaveni)
	except Exception as e:
		pass

	return render_template('nastaveni.html', nasGlobal = globalNastaveni, nasVod = nastaveniVodomer, nasVen = nastaveniVentil, nasEmail = nasEmail, nasNet = netEth)


@app.route('/nastaveni', methods=["POST"])
@login_required
def nastaveniSave():
	formData = request.form
	pulsLitr = formData.get('pulsLitr')
	limitLitry = formData.get('limitLitry')
	limitJednotka = formData.get('limitJednotka')
	predchoziStavLitry = formData.get('predchoziStavLitry')
	releMod = ""#formData.get('releMod')
	ventilMod = ""#formData.get('ventilMod')
	vychoziStav = ""#formData.get('vychoziStav')
	autoMod = formData.get('autoMod')
	newPass = formData.get('newPass')
	telephone = "" #formData.get('telephone')
	voicePass = "" #formData.get('voicePass')
	email = formData.get('email')
	emailAutoSend = formData.get('emailAutoSend')
	emailServer = formData.get('emailServer')
	emailPort = formData.get('emailPort')
	emailSecurity = formData.get('emailSecurity')
	emailUsername = formData.get('emailUsername')
	emailPass = formData.get('emailPass')
	emailFrom = formData.get('emailFrom')
	wsServer = formData.get('wsServer')
	#wsKey = formData.get('wsKey')
	ethIp = formData.get('ethIp')
	ethMask = formData.get('ethMask')
	ethGateway = formData.get('ethGateway')
	apPwd = formData.get('apPwd')
	if len(apPwd) >= 5:
		apconf['wpa_passphrase'] = apPwd
		apconf.write()

	try:
		dataGlob = {"LimitLitry": limitLitry, "LimitJednotka": limitJednotka, "PocatecniStavLitry": predchoziStavLitry, "Email": email, "AutoSendMail": emailAutoSend, 'Tel': telephone, 'HesloTel': voicePass, 'WSServer': wsServer}
		
		dataMail = {"Server": emailServer, "Port": emailPort, "Security": emailSecurity, "Username": emailUsername, "Pass": emailPass, "FromEmail": emailFrom}
		
		dataVenNas = {"ReleMod": releMod, "VentilMod": ventilMod, "VychoziStav": vychoziStav, "AutoMod": autoMod}
		dataVodNas = {"PulsLitr": pulsLitr}
		
		writer.execute('''INSERT INTO nastaveni_global (LimitLitry, LimitJednotka, PocatecniStavLitry, Email, AutoSendMail, Tel, HesloTel, WSServer) VALUES (?,?,?,?,?,?,?,?)''',(dataGlob["LimitLitry"], dataGlob["LimitJednotka"], dataGlob["PocatecniStavLitry"], dataGlob["Email"], dataGlob["AutoSendMail"], dataGlob["Tel"], dataGlob["HesloTel"], dataGlob["WSServer"]))
		
		writer.execute('''INSERT INTO nastaveni_email (Server, Port, Security, Username, Pass, FromEmail) VALUES (?, ?, ?, ?, ?, ?)''',(dataMail["Server"], dataMail["Port"], dataMail["Security"], dataMail["Username"], dataMail["Pass"], dataMail["FromEmail"]))
		
		writer.execute('''INSERT INTO vodomer_nastaveni (PulsLitr) VALUES (?)''',(dataVodNas["PulsLitr"],))
		
		writer.execute('''INSERT INTO ventil_nastaveni (ReleMod, VentilMod, VychoziStav, AutoMod) VALUES (?,?,?,?)''',(dataVenNas["ReleMod"],dataVenNas["VentilMod"],dataVenNas["VychoziStav"],dataVenNas["AutoMod"]))
		
		if newPass != "":	
			heslo = enc_pwd(newPass)
			writer.execute('''UPDATE uzivatele SET Heslo = ? Where Login = ?''',(heslo, "Admin"))
			if emailAutoSend == 1:
				SendMail("Změna přístupového hesla","","<p>Vaše nové přístupové heslo bylo nastaveno na: <b>" + newPass +"</b></p>")
		
		writer.commit()
		
		if len(ethIp) != 0 and len(ethGateway) != 0:
			change_static_ip(ethIp, ethGateway, "8.8.8.8 8.8.4.4")
		else:
			change_static_ip("","","")
		
		#writeWSKey(wsKey)
		flash(u"Nastavení bylo uloženo", "success")
	except Exception as e:
		print(e)
		flash(u"Nastavení nebylo uloženo. > " + str(e), "danger")
		pass
	return redirect(url_for('nastaveni'))


@app.route('/getData')
@compress.compressed()
@login_required
def get_data():
	ventilData = {}
	dataVod = {}
	globNas = {}
	try:
		cur = reader.cursor()
		cur.execute('''SELECT * FROM vodomer_data ORDER BY TsTime DESC LIMIT 1''')
		dataVod = cur.fetchone()
		cur.execute('''SELECT * FROM ventil_data ORDER BY TsTime DESC LIMIT 1''')
		ventilData = cur.fetchone()
		cur.execute('''SELECT * FROM nastaveni_global ORDER BY Ulozeno DESC LIMIT 1''')
		globNas = cur.fetchone()
		stav = 0	
		pocatekL = 0
		jednotka = 1
		limit = 5
		error = ""
		if "ID" in ventilData:
			stav = ventilData['StavVentil']
		
		if "ID" in globNas:
			pocatekL = globNas['PocatecniStavLitry']
			jednotka = globNas['LimitJednotka']
			limit = globNas['LimitLitry']
			
	except Exception as e:
		error = e
		pass
	return jsonify(akt=dataVod,stavVentil=stav,prStavL=pocatekL, jednotka=jednotka, limit=limit)


@app.route('/ventil/1',methods=['GET'])
@login_required
def ventilOtevrit():
	try:
		openVentil()
		flash(u'Ventil - otevřeno','success')
	except Exception as e:
		flash(u'Ventil - se nepodařilo otevřít','danger')
		pass
	return redirect(url_for('index'))


@app.route('/ventil/0',methods=['GET'])
@login_required
def ventilZavrit():
	try:
		closeVentil()
		flash(u'Ventil - zavřeno', 'success')
	except Exception as e:
		flash(u'Ventil - se nepodařilo zavřít', 'danger')
		pass
	return redirect(url_for('index'))


@app.route('/getLogs')
@login_required
def download_logs():
	file_name = "/home/pi/ChytryVodomer/logs.zip"
	createZip(file_name)
	return send_file(file_name, attachment_filename = 'logs.zip', as_attachment = True)


@app.route('/restart',methods=['GET'])
@login_required
def restart():
	os.system("sudo reboot")

# Import z wsagent.py, nelze importovat, problém s vláknováním signálů
def getConfig():
	global config
	config = {
		'ID': 0,
		'LIMIT': 5,
		'JEDNOTKA': 1,
		"WS_SERVER": "device.fengoo.cz/ws/v2",
		"WS_KEY": "",
		"TOTAL_L": 0
	}

	cur = reader.cursor()
	cur.execute('''SELECT * FROM nastaveni_global ORDER BY Ulozeno DESC''')
	globalNastaveni = cur.fetchone()
	cur.close()
	if globalNastaveni:
		if "ID" in globalNastaveni:
			config["ID"] = globalNastaveni["ID"]
			config["LIMIT"] = globalNastaveni["LimitLitry"]
			config["JEDNOTKA"] = globalNastaveni["LimitJednotka"]
			config["WS_SERVER"] = globalNastaveni["WSServer"]
			config["TOTAL_L"] = globalNastaveni["PocatecniStavLitry"]
	with open('/home/pi/info/wskey','r') as f:
		wsKey = f.read()
		wsKey = wsKey.replace("\r","")
		config["WS_KEY"] = wsKey.replace("\n","")


@app.route('/update',methods=['GET'])
@login_required
def updateApp():
	import websocket

	getConfig()
	responseMsg = {
		"event": "updating",
		"params": {
		}
	}

	with open('/home/pi/info/info.json',) as f:
		infoData = json.load(f)
		responseMsg["params"] = {"fwVersion": infoData["NewVersion"]}

	os.system('sudo systemctl stop wsagent.service')

	ADDRESS = "wss://" + config["WS_SERVER"]
	CREDENTIALS = config["WS_KEY"]
	ws = websocket.WebSocket()
	ws.connect(ADDRESS,	header = {'Authorization: Basic ' + base64.b64encode(bytes(CREDENTIALS,'utf-8')).decode('utf-8')})
	ws.send(json.dumps(responseMsg).encode('utf-8'))
	res = subprocess.Popen("sudo sh /home/pi/install.sh 8", shell=True, stdout=subprocess.PIPE).stdout.read()


@app.route('/updated',methods=['GET'])
@login_required
def updatedApp():
	flash(u'Aktualizace - hotovo', 'success')
	return redirect(url_for('nastaveni'))

@app.route('/reset',methods=['GET'])
@login_required
def resetToDefault():
	apconf['wpa_passphrase'] = "Fengoo99"
	apconf.write()
	
	change_static_ip("","","")
	
	res = subprocess.Popen("sudo sh /home/pi/install.sh 10", shell=True, stdout=subprocess.PIPE).stdout.read()
	
	return redirect(url_for('restart'))

@login.unauthorized_handler
def unauthorized():
	write_log("INFO", "Login", "Unauthorized")
	return redirect("/login")

@login.user_loader
def load_user(username):
	cur = reader.cursor()
	cur.execute('''SELECT * FROM uzivatele ORDER BY Ulozeno DESC LIMIT 1''')
	data = cur.fetchone()
	cur.close()

	if data is None:
		return None

	user = User(data['ID'], data['Login'], data['Heslo'], data['Ulozeno'])
	return user

@app.route('/login', methods=['GET', 'POST'])
@compress.compressed()
def login_user_switch():
	if current_user.is_authenticated:
		return redirect('/')
	if request.method == 'POST':
		username = request.form['username']
		password = request.form['password']
		user = load_user(username)

		if user is None or not check_pwd(password, user.heslo):
			return redirect('/login?err=y')

		user.authenticated = True
		user.active = True
		login_user(user)
		return redirect('/')

	return render_template('login.html')

@app.route('/favicon.ico')
def favicon():
	return send_from_directory(os.path.join(app.root_path, 'static', 'images'), 'favicon.ico', mimetype='image/png')

def main():
	write_log("INFO", "Application", "Started")
	app.run(host='0.0.0.0', port=80)

if __name__ == '__main__':
	main()
