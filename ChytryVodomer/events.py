#!/usr/bin/python3
"""events.py: programo pro nastaveni eventu"""
__author__      = "Ladislav Hlousek"
__copyright__   = "Copyright 2018, Ladislav Hlousek"
__version__     = "1.0.0"
__maintainer__  = "Ladislav Hlousek"
__email__       = "ladishlousek@gmail.com"
__status__      = "Production"

import os,sys,signal
import sqlite3
from time import sleep
from datetime import datetime

def signal_handler(signal, frame):
	sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

db = "/home/pi/ChytryVodomer/db/chytryvodomer.db"

reader = sqlite3.connect(db, isolation_level=None, check_same_thread=False)
reader.execute('pragma journal_mode=wal;')
reader.commit()
reader.row_factory = lambda c, r: dict(zip([col[0] for col in c.description], r))

def getStavVentil():
	stav_ventil = 0
	cur = reader.cursor()
	cur.execute('''SELECT * FROM ventil_data ORDER BY TsTime DESC LIMIT 1''')
	ventilData = cur.fetchone()
	cur.close()
	if ventilData is not None:
		stav_ventil = ventilData["StavVentil"]
	
	return stav_ventil

def openVentil(limit, limitVal):
	try:
		if limit != None and limitVal != None:
			cur = reader.cursor()
			cur.execute('''SELECT * FROM nastaveni_global ORDER BY Ulozeno DESC LIMIT 1''')
			globNas = cur.fetchone()
			cur.close()
			reader.execute('''UPDATE nastaveni_global SET LimitLitry = ?, LimitJednotka = ? WHERE ID = ?''',(limitVal, limit, globNas["ID"]))
			reader.commit()
			sleep(0.5)
		
		reader.execute('''INSERT INTO ventil_data (StavVentil, AutoAkce) VALUES (1,4)''')
		reader.commit()
	except:
		pass

def closeVentil():
	try:
		reader.execute('''INSERT INTO ventil_data (StavVentil, AutoAkce) VALUES (0,4)''')
		reader.commit()
	except:
		pass

def findEvent(id):
	event = {}
	cur = reader.cursor()
	cur.execute('''SELECT * FROM events WHERE ID = ?''', (id,))
	event = cur.fetchone()
	cur.close()
	return event

def main():
	cmdArg = ""
	id = ""
	if len(sys.argv) > 2:
		cmdArg = sys.argv[1]
		id = sys.argv[2]
	else:		
		print("-s <id> = start action event id")
		print("-e <id> = end action event id")
		return
	event = findEvent(id)
	if event is not None:
		jednotka = int(str(event["LimitJednotka"]))
		limit = int(str(event["LimitVal"]))
		akce = ""
		if cmdArg == "-s":
			akce = event["StartAkce"]
		if cmdArg == "-e":
			akce = event["KonecAkce"]
		
		if akce == 1:
			closeVentil()
			print("ventil close")
		if akce == 2:
			openVentil(None, None)
			print("ventil open")
		if akce == 3:
			openVentil(jednotka, limit)
			print("ventil open with limit > ", str(jednotka), str(limit))
if __name__ == '__main__':
	main()
