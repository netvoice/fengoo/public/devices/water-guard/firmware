#!/bin/bash
#Verze 1.8

if [ $# -eq 0 ]; then
  echo "Zadejte \"sudo sh .\install.sh 1\" pro instalaci, 2 pro aktualizaci vodomer-hw, 3 - aktualizace pro RPI, 4 - reset DB, logů a služeb, 5 - reset produkce, 6 - resete DEV"
  exit 1
fi

echo "Verze instalatoru 1.8.1"

if [ $1 -eq 1 ]; then
  echo "### INSTALACE ###"
  
  echo "Vyplnte WS klic pro propojeni s Fengoo cloudem"
  read -p 'WS klic: ' WSKey
  echo "Zadany klic ${WSKey}"
  
  echo "Zadejte vychozi stav ventilu 1 = otevreno nebo 0 = zavreno"
  read -p 'Stav (1 / 0): ' VentilDef
  echo "Vychozi stav ventilu bude ${VentilDef}"
  
  #sudo apt-get -y update
  #sudo apt-get -y upgrade
  #sudo apt-get -y update --fix-missing
  #sudo apt-get -y dist-upgrade
  
  sudo apt-get install -y git python3-pip sqlite3 libsqlite3-dev wiringpi hostapd dnsmasq iptables-persistent ntp
  
  sudo pip3 install wifi
  sudo pip3 install flask
  sudo pip3 install flask_mail
  sudo pip3 install flask_httpauth
  sudo pip3 install Flask-Compress
  sudo pip3 install flask-login
  sudo pip3 install passlib
  sudo pip3 install python-crontab
  sudo pip3 install RPi.GPIO
  sudo pip3 install websocket_client
  sudo pip3 install psutil
  sudo pip3 install pyhostapdconf
    
  if [ -d "/home/pi/repo" ]; then
    sudo rm -rf /home/pi/repo
    sudo mkdir /home/pi/repo/
  else
    sudo mkdir /home/pi/repo
  fi

  #echo "Možná Bude potreba zadat prihlasovaci udaje pro repozitar vodomer-hw z gitu: https://gitlab.com/netvoice/fengoo/public/devices/water-guard/firmware.git"
  sudo git clone https://gitlab.com/netvoice/fengoo/public/devices/water-guard/firmware.git /home/pi/repo/

  if [ -d "/home/pi/repo" ]; then
    if [ -d "/home/pi/repo/vodomer-hw" ]; then
      sudo cp -rf /home/pi/repo/vodomer-hw/* /home/pi/
    else
      sudo cp -rf /home/pi/repo/* /home/pi/
    fi
  else
    if [ -d "/home/pi/vodomer-hw" ]; then
      sudo cp -rf /home/pi/vodomer-hw/* /home/pi/
    fi
  fi

  if [ -d "/home/pi/ChytryVodomer" ]; then
    echo "Repozitář OK"
  else
    echo "Chybí repozitář s daty"
    exit 1
  fi
  
  echo "Probíhá instalace..."
  sudo chown -R pi /home/pi/ 
  sudo chmod -R 777 /home/pi/
  
  sudo cp -rf /home/pi/auto-hotspot/hostapd /etc/default/hostapd
  sudo cp -rf /home/pi/auto-hotspot/hostapd.conf /etc/hostapd/hostapd.conf
  sudo cp -rf /home/pi/auto-hotspot/dnsmasq.conf /etc/dnsmasq.conf
  sudo cp -rf /home/pi/auto-hotspot/interfaces /etc/network/interfaces
  sudo cp -rf /home/pi/auto-hotspot/ap /etc/network/interfaces.d/ap
  sudo cp -rf /home/pi/auto-hotspot/10-wpa_supplicant /lib/dhcpcd/dhcpcd-hooks/10-wpa_supplicant
  sudo cp -rf /home/pi/auto-hotspot/90-wireless.rules /etc/udev/rules.d/90-wireless.rules
  echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
  echo 1 > /proc/sys/net/ipv4/ip_forward
  iptables -t nat -A POSTROUTING -s 192.168.4.0/24 ! -d 192.168.4.0/24 -j MASQUERADE
  iptables-save > /etc/iptables/rules.v4

  sudo cp -rf /home/pi/ChytryVodomer/config.txt /boot/
  sudo cp -rf /home/pi/ChytryVodomer/rc.local /etc/
  sudo cp -rf /home/pi/ChytryVodomer/modules /etc/
  sudo cp -rf /home/pi/ChytryVodomer/resolv.conf /etc/
    
  sudo timedatectl set-timezone Europe/Prague
  sudo chmod +x /home/pi/hwcsync/timesync.sh
  sudo sh /home/pi/hwcsync/timesync.sh
  
  sudo mkdir /home/pi/ChytryVodomer/db/
  sudo mkdir /home/pi/ChytryVodomer/logs/
    
  sudo python3 /home/pi/ChytryVodomer/install.py $WSKey $VentilDef
  
  sudo cp -rf /home/pi/wifiman/wifiman.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/ventil.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/btn.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/vodomer.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/webapp.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/wsagent.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/power.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/teplota.service /etc/systemd/system/  
  sudo cp -rf /home/pi/ChytryVodomer/ethmac.service /etc/systemd/system/
  
  sudo rm -rf /home/pi/repo
  
  sudo systemctl enable wifiman.service
  sudo systemctl enable btn.service
  sudo systemctl enable ventil.service
  sudo systemctl enable vodomer.service
  sudo systemctl enable webapp.service
  sudo systemctl enable wsagent.service
  sudo systemctl enable power.service
  sudo systemctl enable teplota.service
  sudo systemctl enable ethmac.service
  
  sudo systemctl unmask hostapd
  sudo systemctl restart hostapd
  sudo systemctl restart dnsmasq
  
  sudo cp -rf /home/pi/auto-hotspot/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
  
  echo "Instalace hotova. Zarizeni se behem par vterin restartuje"
  echo "Zarizeni se po restartu automaticky nastavi do rezimu AP - 192.168.4.1:81 web pro vyber wifi"
  sudo reboot
fi


if [ $1 -eq 2 ]; then
  echo "### AKTUALIZACE ###"
  sudo systemctl stop wifiman.service
  sudo systemctl stop ventil.service
  sudo systemctl stop btn.service
  sudo systemctl stop vodomer.service
  sudo systemctl stop webapp.service
  sudo systemctl stop wsagent.service
  sudo systemctl stop power.service
  sudo systemctl stop teplota.service
  
  #sudo apt-get -y update
  #sudo apt-get -y upgrade
  #sudo apt-get -y update --fix-missing
  #sudo apt-get -y dist-upgrade
  
  if [ -d "/home/pi/repo" ]; then
    sudo rm -rf /home/pi/repo
    sudo mkdir /home/pi/repo/
  else
    sudo mkdir /home/pi/repo/
  fi

  #sudo cp -rf /home/pi/info/info.json /home/pi/info/info.json.bak
  #echo "Bude potreba zadat prihlasovaci udaje pro repozitar vodomer-hw z gitu: https://gitlab.com/netvoice/fengoo/public/devices/water-guard/firmware.git"
  sudo git clone https://gitlab.com/netvoice/fengoo/public/devices/water-guard/firmware.git /home/pi/repo/

  if [ -d "/home/pi/repo" ]; then
    if [ -d "/home/pi/repo/vodomer-hw" ]; then
      sudo cp -rf /home/pi/repo/vodomer-hw/* /home/pi/
    else
      sudo cp -rf /home/pi/repo/* /home/pi/
    fi
  else
    if [ -d "/home/pi/vodomer-hw" ]; then
      sudo cp -rf /home/pi/vodomer-hw/* /home/pi/
    fi
  fi

  if [ -d "/home/pi/ChytryVodomer" ]; then
    echo "Repozitář OK"
  else
    echo "Chybí repozitář s daty"
    exit 1
  fi

  if [ -d "/home/pi/ChytryVodomer/db" ]; then
    echo "DB ok"
  else
    echo "Creating DB"
    sudo mkdir /home/pi/ChytryVodomer/db
    sudo python3 /home/pi/ChytryVodomer/install.py
  fi

  if [ -d "/home/pi/ChytryVodomer/logs" ]; then
    echo "Logs dir OK"
  else
    sudo mkdir /home/pi/ChytryVodomer/logs
  fi

  echo "Probíhá aktualizace..."
  sudo chown -R pi /home/pi/ 
  sudo chmod -R 777 /home/pi/
  
  sudo sh /home/pi/clibs/btn/make.sh
  sudo sh /home/pi/clibs/ventil/make.sh
  sudo sh /home/pi/clibs/vodomer/make.sh
  
  sudo cp -rf /home/pi/auto-hotspot/hostapd /etc/default/hostapd
  sudo cp -rf /home/pi/auto-hotspot/hostapd.conf /etc/hostapd/hostapd.conf
  sudo cp -rf /home/pi/auto-hotspot/dnsmasq.conf /etc/dnsmasq.conf
  sudo cp -rf /home/pi/auto-hotspot/interfaces /etc/network/interfaces
  sudo cp -rf /home/pi/auto-hotspot/ap /etc/network/interfaces.d/ap
  sudo cp -rf /home/pi/auto-hotspot/10-wpa_supplicant /lib/dhcpcd/dhcpcd-hooks/10-wpa_supplicant
  sudo cp -rf /home/pi/auto-hotspot/90-wireless.rules /etc/udev/rules.d/90-wireless.rules
  
  sudo cp -rf /home/pi/ChytryVodomer/config.txt /boot/
  sudo cp -rf /home/pi/ChytryVodomer/rc.local /etc/
  sudo cp -rf /home/pi/ChytryVodomer/modules /etc/
  sudo cp -rf /home/pi/ChytryVodomer/resolv.conf /etc/
    
  sudo timedatectl set-timezone Europe/Prague
  sudo chmod +x /home/pi/hwcsync/timesync.sh
  sudo sh /home/pi/hwcsync/timesync.sh
  
  sudo cp -rf /home/pi/wifiman/wifiman.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/ventil.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/btn.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/vodomer.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/webapp.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/wsagent.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/power.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/teplota.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/ethmac.service /etc/systemd/system/
  
  sudo rm -rf /home/pi/repo
  
  sudo cp -rf /home/static/* /home/pi/ChytryVodomer/
  sudo cp -rf /home/static/* /home/pi/backup/ChytryVodomer/
  
  sudo systemctl daemon-reexec
  sudo reboot
fi

if [ $1 -eq 3 ]; then
  echo "### UPDATE RPI ###"
  sudo apt-get -y update
  sudo apt-get -y upgrade
  sudo apt-get -y update --fix-missing
  sudo apt-get -y dist-upgrade
  exit 1
fi

if [ $1 -eq 4 ]; then
  echo "### STOP SLUZEB ###"
  sudo systemctl stop ventil.service
  sudo systemctl stop btn.service
  sudo systemctl stop vodomer.service
  sudo systemctl stop webapp.service
  sudo systemctl stop wsagent.service
  sudo systemctl stop power.service
  sudo systemctl stop teplota.service

  echo "### RESET LOG & DB ###"
  
  sudo rm -rf /home/pi/ChytryVodomer/logs/*
  sudo rm -rf /home/pi/ChytryVodomer/db/*
  sudo python3 /home/pi/ChytryVodomer/install.py

  echo "### START SLUZEB ###"  
  sudo systemctl start vodomer.service
  sudo systemctl start ventil.service
  sudo systemctl start btn.service  
  sudo systemctl start webapp.service
  sudo systemctl start wsagent.service
  sudo systemctl start power.service
  sudo systemctl start teplota.service
  exit 1
fi
  
if [ $1 -eq 5 ]; then
  echo "### RESET PRO Produkci ###"
  echo "### STOP SLUZEB ###"
  sudo systemctl stop ventil.service
  sudo systemctl stop btn.service
  sudo systemctl stop vodomer.service
  sudo systemctl stop webapp.service
  sudo systemctl stop wsagent.service
  
  echo "### RESET LOG & DB ###"
    
  sudo rm -rf /home/pi/ChytryVodomer/logs/*
  sudo rm -rf /home/pi/ChytryVodomer/db/*
  sudo python3 /home/pi/ChytryVodomer/install.py
  sudo cp -rf /home/pi/auto-hotspot/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
    
  sudo chown -R pi /home/pi/ 
  sudo chmod -R 777 /home/pi/
  echo "### HOTOVO! VYPNETE ZARIZENI###"
fi

if [ $1 -eq 6 ]; then
  sudo timedatectl set-timezone Europe/Prague
  sudo chmod +x /home/pi/hwcsync/timesync.sh
  sudo sh /home/pi/hwcsync/timesync.sh
fi

if [ $1 -eq 8 ]; then
	if [ -d "/home/pi/repo" ]; then
		sudo rm -rf /home/pi/repo
		sudo mkdir /home/pi/repo/
	else
		sudo mkdir /home/pi/repo/
	fi
	sudo pip3 install websocket_client --upgrade
	sudo pip3 install flask-login
	sudo cp -rf /home/pi/info/wskey /home/pi/wskey
	#echo "Bude potreba zadat prihlasovaci udaje pro repozitar vodomer-hw z gitu: https://gitlab.com/netvoice/fengoo/public/devices/water-guard/firmware.git"
	sudo git clone https://gitlab.com/netvoice/fengoo/public/devices/water-guard/firmware.git /home/pi/repo/

	if [ -d "/home/pi/repo" ]; then
		if [ -d "/home/pi/repo/vodomer-hw" ]; then
			sudo cp -rf /home/pi/repo/vodomer-hw/* /home/pi/
		else
			sudo cp -rf /home/pi/repo/* /home/pi/
		fi
	else
		if [ -d "/home/pi/vodomer-hw" ]; then
			sudo cp -rf /home/pi/vodomer-hw/* /home/pi/
		fi
	fi

	if [ -d "/home/pi/ChytryVodomer" ]; then
		echo "Repozitář OK"
	else
		echo "Chybí repozitář s daty"
		exit 1
	fi
  
  sudo cp -rf /home/static/* /home/pi/ChytryVodomer/static/
  sudo cp -rf /home/static/* /home/pi/backup/ChytryVodomer/static/
  
  sudo cp -rf /home/pi/wskey /home/pi/info/wskey
  sudo rm -rf /home/pi/wskey

  echo "Probíhá aktualizace..."
  #sudo apt-get -y update
  #sudo apt-get -y dist-upgrade
  
  sudo chown -R pi /home/pi/ 
  sudo chmod -R 777 /home/pi/
  
  sudo cp -rf /home/pi/auto-hotspot/hostapd /etc/default/hostapd
  sudo cp -rf /home/pi/auto-hotspot/hostapd.conf /etc/hostapd/hostapd.conf
  sudo cp -rf /home/pi/auto-hotspot/dnsmasq.conf /etc/dnsmasq.conf
  sudo cp -rf /home/pi/auto-hotspot/interfaces /etc/network/interfaces
  sudo cp -rf /home/pi/auto-hotspot/ap /etc/network/interfaces.d/ap
  sudo cp -rf /home/pi/auto-hotspot/10-wpa_supplicant /lib/dhcpcd/dhcpcd-hooks/10-wpa_supplicant
  sudo cp -rf /home/pi/auto-hotspot/90-wireless.rules /etc/udev/rules.d/90-wireless.rules
  echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
  echo 1 > /proc/sys/net/ipv4/ip_forward
  iptables -t nat -A POSTROUTING -s 192.168.4.0/24 ! -d 192.168.4.0/24 -j MASQUERADE
  iptables-save > /etc/iptables/rules.v4

  sudo cp -rf /home/pi/ChytryVodomer/config.txt /boot/
  sudo cp -rf /home/pi/ChytryVodomer/rc.local /etc/
  sudo cp -rf /home/pi/ChytryVodomer/modules /etc/
  sudo cp -rf /home/pi/ChytryVodomer/resolv.conf /etc/
  
  sudo timedatectl set-timezone Europe/Prague
  sudo chmod +x /home/pi/hwcsync/timesync.sh
  sudo sh /home/pi/hwcsync/timesync.sh
  
  sudo cp -rf /home/pi/wifiman/wifiman.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/ventil.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/btn.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/vodomer.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/webapp.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/wsagent.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/power.service /etc/systemd/system/
  sudo cp -rf /home/pi/ChytryVodomer/teplota.service /etc/systemd/system/ 
  
  sudo rm -rf /home/pi/repo
  
  sudo chown -R pi /home/pi/ 
  sudo chmod -R 777 /home/pi/
 
  echo -n > /home/pi/info/updated
  sudo reboot
fi

if [ $1 -eq 9 ]; then
  sudo systemctl stop ventil.service
  sudo systemctl stop vodomer.service
  sudo systemctl stop wsagent.service
  
  sudo chown -R pi /home/pi/
  sudo chmod -R 777 /home/pi/
    
  if [ -d "/home/temp/info" ]; then
    sudo cp -rf /home/pi/info/* /home/temp/info/
  else
    sudo mkdir /home/temp/info/
    sudo cp -rf /home/pi/info/* /home/temp/info/
  fi
  
  sudo cp -rf /home/pi/backup/* /home/pi/

  sudo cp -rf /home/static/* /home/pi/ChytryVodomer/static/
  sudo cp -rf /home/static/* /home/pi/backup/ChytryVodomer/static/
  
  sudo cp -rf /home/temp/info/* /home/pi/info/

  sudo chown -R pi /home/pi/
  sudo chmod -R 777 /home/pi/
    
  sudo systemctl restart webapp.service
  
  sudo reboot
fi

if [ $1 -eq 10 ]; then
  sudo systemctl stop ventil.service
  sudo systemctl stop vodomer.service
  sudo systemctl stop wsagent.service
  sudo systemctl stop btn.service
  
  sudo chown -R pi /home/pi/
  sudo chmod -R 777 /home/pi/
    
  sudo rm -rf /home/pi/ChytryVodomer/logs/*
  sudo rm -rf /home/pi/ChytryVodomer/db/*
  
  sudo python3 /home/pi/ChytryVodomer/install.py
  
  sudo cp -rf /home/pi/auto-hotspot/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
fi

#check for new version
if [ $1 -eq 11 ]; then
  sudo git ls-remote --tags https://gitlab.com/netvoice/fengoo/public/devices/water-guard/firmware.git > /home/pi/info/versions.txt
fi
